import { Stack, Typography } from "@mui/material";
import React, { ReactNode } from "react";
import { CiUser } from "react-icons/ci";
import { IFakeStudent } from "../pages/student/services";

interface IProps {
  Icon: ReactNode;
  label: string;
  value: any;
  subtitle?: string;
}
export default function ProfileInfoCard({
  Icon,
  label,
  value,
  subtitle,
}: IProps) {
  return (
    <Stack width="100%">
      <Typography variant="body2">{label}</Typography>
      <Stack
        borderRadius={(theme) => theme.spacing(0.5)}
        border={(theme) =>
          `1px solid ${theme.palette.action.disabledBackground}`
        }
        direction="row"
        spacing={0.85}
        padding={0.5}
        alignItems="center"
        justifyContent="flex-start"
      >
        {Icon}
        <Typography variant="body2">{value}</Typography>
      </Stack>
      {subtitle ? <Typography variant="caption">{subtitle}</Typography> : null}
    </Stack>
  );
}
