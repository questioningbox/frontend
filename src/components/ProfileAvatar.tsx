import { Stack, StackProps, Typography } from "@mui/material";
import React from "react";
import { useAppSelector } from "../app/hooks";
import UserAvatar from "./UserAvatar";

interface IProps {
  props?: StackProps;
  size?: "small" | "medium" | "large";
  profileUrl?: string;
  name?: string;
  width?: number;
  height?: number;
}
export default function ProfileAvatar({
  props,
  size,
  name,
  profileUrl,
  height,
  width,
}: IProps) {
  return (
    <>
      {profileUrl && (
        <UserAvatar size={size ? size : "small"} source={profileUrl} />
      )}

      {name && (
        <Stack
          sx={(theme) => ({
            width: width ? width : "40px",
            height: height ? height : "40px",
            borderRadius: "100%",
            border: `1px solid ${theme.palette.action.hover}`,
            alignItems: "center",
            justifyContent: "center",
          })}
        >
          <Typography
            fontWeight="bold"
            sx={(theme) => ({
              textTransform: "uppercase",
            })}
            variant="body1"
            color="primary"
          >
            {name.charAt(0)}
          </Typography>
        </Stack>
      )}
    </>
  );
}
