import { IconButton, Stack, StackProps, Typography } from "@mui/material";
import React, { ReactNode } from "react";

interface IProps {
  handleClick?: () => void;
  children?: ReactNode;
  label?: string;
  props?: StackProps;
}
export default function CustomInkButton({
  handleClick,
  label,
  children,
  props,
}: IProps) {
  return (
    <Stack
      direction="row"
      spacing={1}
      alignItems="center"
      justifyContent="center"
      {...props}
    >
      {label && <Typography variant="body2">{label}</Typography>}
      <IconButton onClick={handleClick}>{children}</IconButton>
    </Stack>
  );
}
