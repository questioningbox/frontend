import { IconButton, Stack, Typography } from "@mui/material";
import React from "react";
import { BsFileEarmarkText } from "react-icons/bs";
import { MdOutlineEdit } from "react-icons/md";
import { RiDeleteBin6Line } from "react-icons/ri";
import { AppColors } from "../constants";

export default function QuestionCard() {
  return (
    <Stack
      alignItems="center"
      direction="row"
      width="100%"
      borderRadius={(theme) => theme.spacing(0.5)}
      border={(theme) => `1px solid ${theme.palette.action.hover}`}
      padding={(theme) => theme.spacing(1.5, 1)}
    >
      <BsFileEarmarkText
        style={{
          backgroundColor: "#f0f0f0",
          borderRadius: "30px",
          padding: "5px",
          fontSize: "20px",
        }}
        color={AppColors.primary}
        size={24}
      />
      <Stack paddingLeft={1}>
        <Typography fontSize={(theme) => theme.spacing(1.85)} variant="body2">
          QuestionSet.xlsx
        </Typography>
        <Typography variant="caption">
          {Math.floor(Math.random() * 250)}kb
        </Typography>
      </Stack>
      <Stack flex={1} />
      <Stack direction="row" paddingX={1.5} spacing={1}>
        <IconButton size="small">
          <MdOutlineEdit />
        </IconButton>
        <IconButton size="small">
          <RiDeleteBin6Line size={16} style={{ color: "#c0c0c0" }} />
        </IconButton>
      </Stack>
    </Stack>
  );
}
