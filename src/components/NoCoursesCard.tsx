import { Stack, Typography } from "@mui/material";
import React from "react";
import { SiDatabricks } from "react-icons/si";
export default function NoCoursesCard() {
  return (
    <Stack
      height="100%"
      width="100%"
      padding={2}
      alignItems="center"
      justifyContent="center"
    >
      <SiDatabricks color="#d0d0d0" size={200} />
      <Typography variant="body1">No Courses Available</Typography>
    </Stack>
  );
}
