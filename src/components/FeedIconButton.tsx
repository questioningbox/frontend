import { Chip, IconButton, SvgIconTypeMap, Typography } from "@mui/material";
import { OverridableComponent } from "@mui/material/OverridableComponent";
import React, { ReactElement, ReactNode } from "react";
import { IconType } from "react-icons";

interface IProps {
  Icon: OverridableComponent<SvgIconTypeMap<{}, "svg">> | IconType;
  label: string;
  handleClick?: () => void;
}
export default function FeedIconButton({ Icon, label, handleClick }: IProps) {
  return (
    <IconButton
      sx={(theme) => ({
        alignItems: "center",
        justifyContent: "center",
      })}
      size="small"
      onClick={handleClick}
    >
      <Icon fontSize="small" />
      <Typography variant="caption" fontSize={(theme) => theme.spacing(1.45)}>
        {label}
      </Typography>
    </IconButton>
  );
}
