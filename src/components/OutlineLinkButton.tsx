import { Link, LinkProps } from "@mui/material";
import React from "react";

interface IProps {
  route?: string;
  text: string;
  props?: LinkProps;
}
export default function OutlineLinkButton({ route, text, props }: IProps) {
  return (
    <Link
      sx={(theme) => ({
        border: `1px solid ${theme.palette.action.disabledBackground}`,
        bgcolor: theme.palette.common.white,
        padding: theme.spacing(0.45, 1),
        borderRadius: theme.spacing(0.5),
        fontSize: theme.spacing(1.45),
        color: theme.palette.common.black,
        textDecoration: "none",
      })}
      href={route ? route : ""}
    >
      {text}
    </Link>
  );
}
