export interface IDialogBaseProps {
  open: boolean;
  handleClose: () => void;
}

export interface IFooterLink {
  title: string;
  route: string;
}

export interface IHomeCategoriesLink {
  title: string;
  route: string;
}
