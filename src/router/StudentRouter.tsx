import React from "react";
import { Route } from "react-router-dom";
import {
  EmailNotificationSettingsPage,
  StudentPage,
  StudentProfileEditPage,
  StudentProfileGeneralPage,
  StudentProfilePage,
} from "../pages/student/views";
import NavigationRoutes from "../routes/NavigationRoutes";
import { AuthGuard } from "../pages/auth/view";

export default function StudentRouter() {
  return (
    <Route path={NavigationRoutes.student.profile} element={<StudentPage />}>
      <Route path="" element={<StudentProfilePage />} />
      <Route
        path={NavigationRoutes.student.general}
        element={<StudentProfileGeneralPage />}
      />
      <Route
        path={NavigationRoutes.student.edit}
        element={<StudentProfileEditPage />}
      />
      <Route
        path={NavigationRoutes.student.notifications}
        element={<EmailNotificationSettingsPage />}
      />
    </Route>
  );
}
