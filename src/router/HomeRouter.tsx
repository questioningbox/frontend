import React from "react";
import {
  ContentPage,
  CourseDetailsPage,
  HomePage,
  StudentsPage,
  TeachersPage,
} from "../pages/home/view";
import { Routes, Route } from "react-router-dom";
import NavigationRoutes from "../routes/NavigationRoutes";
import { QuestionsPage } from "../pages/questions/views";
import PreptestRouter from "./PreptestRouter";
import PricingRouter from "./PricingRouter";
import QuestionsRouter from "./QuestionsRouter";
import StudentRouter from "./StudentRouter";
import ChallegeRouter from "./ChallegeRouter";
import GroupsRouter from "./GroupsRouter";
export default function HomeRouter() {
  return (
    <Routes>
      <Route path={NavigationRoutes.home.root} element={<HomePage />}>
        <Route path={NavigationRoutes.home.landing} element={<ContentPage />} />
        <Route
          path={NavigationRoutes.home.students}
          element={<StudentsPage />}
        />
        <Route
          path={NavigationRoutes.home.teachers}
          element={<TeachersPage />}
        />
        <Route
          path={NavigationRoutes.home.course}
          element={<CourseDetailsPage />}
        />
        <Route
          path={NavigationRoutes.home.questions}
          element={<QuestionsPage />}
        />
        {PreptestRouter()}
        {PricingRouter()}
        {QuestionsRouter()}
        {StudentRouter()}
        {ChallegeRouter()}
        {GroupsRouter()}
      </Route>
    </Routes>
  );
}
