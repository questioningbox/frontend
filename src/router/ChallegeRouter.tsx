import React from "react";
import { Route } from "react-router-dom";
import {
  ChallengeDetailsPage,
  ChallengeLeaderBoardPage,
  ChallengeParticipantsPage,
  ReceivedChallengesPage,
  SentChallengesPage,
  UserChallengesPage,
} from "../pages/challenge/views";
import NavigationRoutes from "../routes/NavigationRoutes";

export default function ChallegeRouter() {
  return (
    <React.Fragment>
      <Route
        path={NavigationRoutes.challenge.all}
        element={<UserChallengesPage />}
      />
      <Route
        path={NavigationRoutes.challenge.received}
        element={<ReceivedChallengesPage />}
      />
      <Route
        path={NavigationRoutes.challenge.sent}
        element={<SentChallengesPage />}
      />
      <Route
        path={NavigationRoutes.challenge.leaderboard}
        element={<ChallengeLeaderBoardPage />}
      />
      <Route
        path={NavigationRoutes.challenge.parcipants}
        element={<ChallengeParticipantsPage />}
      />
      <Route
        path={`${NavigationRoutes.challenge.details}`}
        element={<ChallengeDetailsPage />}
      />
    </React.Fragment>
  );
}
