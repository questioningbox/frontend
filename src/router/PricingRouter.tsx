import React from "react";
import { Route } from "react-router-dom";
import { ContentPage, RootPage } from "../pages/pricing/view";
import NavigationRoutes from "../routes/NavigationRoutes";

export default function PricingRouter() {
  return (
    <Route path={NavigationRoutes.pricing.root} element={<RootPage />}>
      <Route path="" element={<ContentPage />} />
    </Route>
  );
}
