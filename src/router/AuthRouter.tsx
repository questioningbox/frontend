import React from "react";
import { Routes, Route } from "react-router-dom";
import {
  AuthPage,
  RegisterByPhonePage,
  ResetPasswordPage,
  ForgotPasswordPage,
  RegisterPage,
  LoginPage,
} from "../pages/auth/view";
import OtpVerificationPage from "../pages/auth/view/OtpVerificationPage";

import NavigationRoutes from "../routes/NavigationRoutes";
export default function AuthRouter() {
  return (
    <Routes>
      <Route path={NavigationRoutes.account.root} element={<AuthPage />}>
        <Route path={NavigationRoutes.account.login} element={<LoginPage />} />
        <Route
          path={NavigationRoutes.account.otpVerification}
          element={<OtpVerificationPage />}
        />
        <Route
          path={NavigationRoutes.account.resetPassword}
          element={<ResetPasswordPage />}
        />
        <Route
          path={NavigationRoutes.account.forgotPassword}
          element={<ForgotPasswordPage />}
        />
        <Route
          path={NavigationRoutes.account.registerByPhone}
          element={<RegisterByPhonePage />}
        />
        <Route
          path={NavigationRoutes.account.register}
          element={<RegisterPage />}
        />
      </Route>
    </Routes>
  );
}
