import React from "react";
import { Route } from "react-router-dom";
import { AboutGroupPage, GroupsPage } from "../pages/group/view";
import NavigationRoutes from "../routes/NavigationRoutes";

export default function GroupsRouter() {
  return (
    <React.Fragment>
      <Route path={NavigationRoutes.groups.root} element={<GroupsPage />} />
      <Route path={NavigationRoutes.groups.info} element={<AboutGroupPage />} />
    </React.Fragment>
  );
}
