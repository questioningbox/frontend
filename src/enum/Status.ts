export enum Status {
  Active = "active",
  InActive = "inactive",
  Suspended = "suspended",
  Blocked = "blocked",
  Pending = "Pending",
  Approved = "approved",
  Completed = "completed",
}
