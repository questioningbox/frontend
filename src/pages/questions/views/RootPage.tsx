import { Stack } from "@mui/material";
import React from "react";
import { Outlet } from "react-router-dom";

export default function RootPage() {
  return (
    <Stack
      height="100vh"
      sx={(theme) => ({
        overflowY: "auto",
      })}
      width="100%"
    >
      <Outlet />
    </Stack>
  );
}
