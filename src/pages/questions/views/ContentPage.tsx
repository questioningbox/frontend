import {
  Chip,
  Stack,
  TextField,
  Typography,
  useMediaQuery,
  Grid,
  IconButton,
  MenuItem,
  Button,
  LinearProgress,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { BsPlus } from "react-icons/bs";
import { CustomModal } from "../../../shared";
import { getCategories, getStoreItems } from "../../../utilities";
import { CourseInfoCard, ServiceUpgradeView, StoreItem } from "../../../views";
import { StoreSection } from "../../home/section";
import { IoIosClose } from "react-icons/io";
import {
  AiFillCheckCircle,
  AiOutlineCloudUpload,
  AiOutlineFileExcel,
} from "react-icons/ai";
import { AppColors } from "../../../constants";
import NavigationRoutes from "../../../routes/NavigationRoutes";
import { OutlineLinkButton } from "../../../components";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { CourseThunk } from "../../../functions/slice";
import ApiRoutes from "../../../routes/ApiRoutes";
export default function ContentPage() {
  const isMobile = useMediaQuery("(max-width:567px)");
  const [modal, setModal] = useState<boolean>(false);
  const { courses } = useAppSelector((state) => state.CoursesReducer);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(
      CourseThunk({
        method: "get",
        url: ApiRoutes.course.crud,
        params: { page: 1, pageSize: 20 },
      })
    );
  }, []);
  return (
    <Stack
      sx={(theme) => ({
        overflowY: "auto",
      })}
      padding={1}
      width="100%"
      height="100%"
    >
      <CustomModal
        props={{
          maxWidth: "sm",
          open: modal,
          sx: (theme) => ({
            bgcolor: theme.palette.action.hover,
          }),
          fullWidth: isMobile,
        }}
        Header={
          <Stack
            direction="row"
            alignItems="center"
            justifyContent="space-between"
            width="100%"
          >
            <Typography fontWeight={700} variant="body1">
              Add Questions
            </Typography>
            <IconButton onClick={() => setModal(false)} size="small">
              <IoIosClose />
            </IconButton>
          </Stack>
        }
        open={modal}
        DialogAction={
          <Stack
            width="100%"
            alignItems="center"
            justifyContent="flex-end"
            spacing={1.5}
            direction="row"
            paddingX={2}
            paddingBottom={2}
          >
            <Button
              sx={(theme) => ({
                textTransform: "none",
              })}
              variant="outlined"
              size="small"
              color="inherit"
            >
              Cancel
            </Button>
            <Button
              sx={(theme) => ({
                textTransform: "none",
              })}
              variant="contained"
              size="small"
              color="primary"
            >
              Submit
            </Button>
          </Stack>
        }
      >
        <Stack spacing={1} width={isMobile ? "90%" : "270px"} minHeight="300px">
          <Stack width="100%">
            <Typography variant="body2">Question Title*</Typography>
            <TextField
              variant="outlined"
              size="small"
              placeholder="Question Title"
            />
          </Stack>
          <Stack width="100%">
            <Typography variant="body2">Select Category*</Typography>
            <TextField
              variant="outlined"
              size="small"
              placeholder="Question Title"
              select
            >
              <MenuItem value="">None</MenuItem>
              {getCategories().map((cate, i) => (
                <MenuItem key={i.toString()} value={cate}>
                  {cate}
                </MenuItem>
              ))}
            </TextField>
          </Stack>
          <Stack>
            <Typography variant="body2">Description</Typography>
            <TextField
              variant="outlined"
              size="small"
              multiline
              minRows={2}
              placeholder="Question Title"
            />
          </Stack>
          <Stack
            padding={1}
            borderRadius={(theme) => theme.spacing(0.85)}
            alignItems="center"
            justifyContent="center"
            border={(theme) => `1px solid ${theme.palette.action.hover}`}
          >
            <IconButton size="small">
              <AiOutlineCloudUpload />
            </IconButton>
            <Typography
              fontSize={(theme) => theme.spacing(1.45)}
              variant="body2"
            >
              Click to upload or Drag and drop
            </Typography>
            <Typography
              fontSize={(theme) => theme.spacing(1.25)}
              variant="caption"
            >
              Excel File {"(.csv,.xlsx)"}
            </Typography>
          </Stack>
          <Stack
            padding={1}
            borderRadius={(theme) => theme.spacing(0.5)}
            alignItems="center"
            justifyContent="center"
            border={(theme) => `1px solid ${theme.palette.primary.main}`}
          >
            <Stack
              direction="row"
              alignItems="center"
              justifyContent="center"
              width="100%"
            >
              <AiOutlineFileExcel
                size={12}
                style={{ color: AppColors.primary }}
              />
              <Typography
                fontSize={(theme) => theme.spacing(1.45)}
                variant="caption"
              >
                Questionbanner.xlsx
              </Typography>
              <Stack flex={1} />
              <AiFillCheckCircle
                style={{ color: AppColors.primary }}
                size={14}
              />
            </Stack>
            <Stack
              paddingLeft={2}
              direction="row"
              alignItems="center"
              justifyContent="flex-end"
              width="100%"
              spacing={0.5}
            >
              <Stack flex={1}>
                <LinearProgress
                  defaultValue={0}
                  color="primary"
                  variant="determinate"
                  valueBuffer={100}
                  value={100}
                  sx={(theme) => ({
                    borderRadius: theme.spacing(2),
                  })}
                />
              </Stack>
              <Typography variant="caption">100%</Typography>
            </Stack>
          </Stack>
        </Stack>
      </CustomModal>
      <ServiceUpgradeView />
      <Stack
        alignItems="center"
        justifyContent="space-between"
        spacing={1.5}
        direction={isMobile ? "column" : "row"}
        width={"100%"}
        padding={(theme) => theme.spacing(1.5, 6)}
      >
        <TextField
          fullWidth={isMobile}
          variant="outlined"
          size="small"
          placeholder="search...."
          sx={(theme) => ({
            minWidth: isMobile ? "100%" : "200px",
          })}
        />
        <Stack
          spacing={1}
          direction="row"
          alignItems="center"
          justifyContent="center"
        >
          <Chip
            size="medium"
            color="primary"
            sx={(theme) => ({
              bgcolor: theme.palette.primary.main,
              color: theme.palette.common.white,
              borderRadius: theme.spacing(0.5),
              width: isMobile ? "100%" : "200px",
            })}
            onClick={() => setModal(true)}
            avatar={
              <BsPlus style={{ background: "transparent" }} color="#fff" />
            }
            label={<Typography variant="caption">Add new questions</Typography>}
          />
          <OutlineLinkButton
            text="Questions"
            route={NavigationRoutes.questions.myquestions}
          />
        </Stack>
      </Stack>
      <Stack padding={(theme) => theme.spacing(1, 6)} width="100%">
        <Grid
          container
          alignItems="center"
          justifyContent="center"
          width="100%"
          spacing={1}
          height="100%"
        >
          {courses.results.map((item) => (
            <CourseInfoCard
              info={item}
              handleMore={() => {}}
              parentProps={{ borderRadius: (theme) => theme.spacing(1) }}
            />
          ))}
        </Grid>
      </Stack>
    </Stack>
  );
}
