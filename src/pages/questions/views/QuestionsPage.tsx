import { Divider, Stack, Typography } from "@mui/material";
import React from "react";
import { AiOutlineCloudUpload } from "react-icons/ai";
import { BsPlus } from "react-icons/bs";
import { CustomIconButton } from "../../../components";
import QuestionCard from "../../../components/QuestionCard";
import { AppColors } from "../../../constants";
import { AppFooter } from "../../../views";

export default function QuestionsPage() {
  return (
    <Stack
      sx={(theme) => ({
        overflowY: "auto",
      })}
      width="100%"
      height="100%"
      padding={(theme) => theme.spacing(1, 6)}
    >
      <Stack
        direction="row"
        alignItems="center"
        justifyContent="space-between"
        paddingY={1}
        marginBottom={1}
        sx={(theme) => ({
          [theme.breakpoints.down("sm")]: {
            width: "100%",
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          },
        })}
      >
        <Typography variant="body1" fontWeight={900}>
          Question Set
        </Typography>
        <Stack
          spacing={1}
          direction="row"
          alignItems="center"
          justifyContent="flex-end"
          sx={(theme) => ({
            [theme.breakpoints.down("sm")]: {
              marginTop: theme.spacing(1),
            },
          })}
        >
          <CustomIconButton
            iconSize={13}
            Icon={BsPlus}
            props={{
              sx: (theme) => ({
                border: `1px solid ${theme.palette.action.hover}`,
                bgcolor: theme.palette.action.hover,
                color: theme.palette.common.black,
                borderRadius: theme.spacing(0.5),
              }),
              color: "default",
            }}
            title="Add questions manually"
            iconColor={AppColors.darkbg}
            handleClick={() => {}}
          />
          <CustomIconButton
            Icon={AiOutlineCloudUpload}
            title="Import CSV file"
            iconSize={10}
            handleClick={() => {}}
          />
        </Stack>
      </Stack>
      <Divider />
      <Stack marginY={1.5} spacing={1}>
        {Array.from({ length: 15 }).map(() => (
          <QuestionCard key={Math.random()} />
        ))}
      </Stack>
      <AppFooter />
    </Stack>
  );
}
