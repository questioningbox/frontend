import { Divider, IconButton, Stack, Typography } from "@mui/material";
import dayjs from "dayjs";
import moment from "moment";
import React, { ReactNode, useEffect, useState } from "react";
import { AiOutlineLike, AiFillLike, AiOutlineComment } from "react-icons/ai";
import { useAppSelector } from "../../../app/hooks";
import { FeedIconButton } from "../../../components";
import { AppColors } from "../../../constants";
import controller from "../../../controller";
import FeedModel from "../../../models/FeedModel";
import ProfileModel from "../../../models/ProfileModel";
import UserModel from "../../../models/UserModel";

interface IProps {
  children: ReactNode;
  info: FeedModel;
  handleLike: () => void;
  handleComment: () => void;
  canComment?: boolean;
}
export default function Feed({
  children,
  info,
  handleLike,
  handleComment,
  canComment,
}: IProps) {
  const [userInfo, setUserInfo] = useState<UserModel | null>(null);
  const { user } = useAppSelector((state) => state.UserReducer);
  const [profileInfo, setProfileInfo] = useState<ProfileModel | null>(null);
  async function getInfo() {
    const uInfo = await controller<UserModel>({
      method: "get",
      url: `feed/user/info/${info.userId}`,
      token: "",
      data: null,
    });
    setUserInfo(uInfo);
  }

  async function getProfile() {
    const uInfo = await controller<ProfileModel>({
      method: "get",
      url: `feed/user/profile/${info.userId}`,
      token: "",
      data: null,
    });
    setProfileInfo(uInfo);
  }

  useEffect(() => {
    getInfo();
    getProfile();
  }, []);

  return (
    <Stack
      width="100%"
      direction="row"
      alignItems="center"
      justifyContent="flex-start"
      padding={1}
      spacing={1}
    >
      <Stack
        width="35px"
        height="35px"
        borderRadius="35px"
        bgcolor={(theme) => theme.palette.action.hover}
        position="relative"
      >
        <Stack
          width="12px"
          height="12px"
          borderRadius="12px"
          position="absolute"
          right={0}
          bottom={-2.5}
          bgcolor="green"
          zIndex={101}
        />
        <Stack
          width="100%"
          height="100%"
          overflow="hidden"
          borderRadius={"100%"}
        >
          {profileInfo && (
            <img alt="feed-avatar" src={profileInfo.profileImage.secureUrl} />
          )}
        </Stack>
      </Stack>
      <Stack flex={1}>
        <Stack
          width="100%"
          spacing={1}
          direction="row"
          alignItems="center"
          justifyContent="flex-start"
        >
          <Typography variant="caption">
            {userInfo ? userInfo.name : ""}
          </Typography>
          <Typography
            fontSize={(theme) => theme.spacing(1.025)}
            variant="caption"
          >
            {/* {moment(info.dateAdded).fromNow()} */}
          </Typography>
        </Stack>
        <Stack width="100%">{children}</Stack>
        <Stack
          padding={(theme) => theme.spacing(0.25, 0)}
          alignItems="center"
          justifyContent="flex-start"
          direction="row"
          spacing={1}
        >
          <Typography variant="caption">
            {dayjs(info.createdAt).format("dd, DD/MM/YYYY hh:mm a")}
          </Typography>
          <FeedIconButton
            handleClick={handleLike}
            label={info.likes.length.toString()}
            Icon={
              info.likes.includes(user ? user.userId : "")
                ? AiFillLike
                : AiOutlineLike
            }
          />
          {canComment && (
            <FeedIconButton
              handleClick={handleComment}
              label={info.comments.length.toString()}
              Icon={AiOutlineComment}
            />
          )}
        </Stack>
        <Divider />
      </Stack>
    </Stack>
  );
}
