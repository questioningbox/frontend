import { Button, Stack } from "@mui/material";
import React from "react";
import { useNavigate } from "react-router-dom";
import { IHomeCategoriesLink } from "../../../interface";
import resources from "../../../resources";

interface IProps {
  info: IHomeCategoriesLink;
}
export default function CategoryCard({ info }: IProps) {
  const navigation = useNavigate();
  return (
    <Stack spacing={0.5} alignItems="center" justifyContent="center">
      <Stack
        width="45px"
        height="45px"
        borderRadius="100%"
        overflow="hidden"
        bgcolor={(theme) => theme.palette.action.hover}
      >
        <img alt="cate-imge" src={resources.cate} />
      </Stack>
      <Button
        sx={(theme) => ({
          textTransform: "none",
        })}
        size="small"
        variant="text"
        color="inherit"
        onClick={() => navigation(info.route)}
      >
        {info.title}
      </Button>
    </Stack>
  );
}
