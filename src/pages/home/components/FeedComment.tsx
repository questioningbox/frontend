import { Divider, Stack, Typography } from "@mui/material";
import dayjs from "dayjs";
import React, { useEffect, useState } from "react";
import { UserAvatar } from "../../../components";
import controller from "../../../controller";
import { FeedCommentModel } from "../../../models";
import ProfileModel from "../../../models/ProfileModel";
import UserModel from "../../../models/UserModel";

interface IProps {
  comment: FeedCommentModel;
}
export default function FeedComment({ comment }: IProps) {
  const [userInfo, setUserInfo] = useState<UserModel | null>(null);
  const [profileInfo, setProfileInfo] = useState<ProfileModel | null>(null);
  async function getInfo() {
    const uInfo = await controller<UserModel>({
      method: "get",
      url: `feed/user/info/${comment.userId}`,
      token: "",
      data: null,
    });
    setUserInfo(uInfo);
  }

  async function getProfile() {
    const uInfo = await controller<ProfileModel>({
      method: "get",
      url: `feed/user/profile/${comment.userId}`,
      token: "",
      data: null,
    });
    setProfileInfo(uInfo);
  }

  useEffect(() => {
    getInfo();
    getProfile();
  }, []);
  return (
    <Stack>
      <Stack marginBottom={0.5} spacing={1.5} direction="row">
        {profileInfo && profileInfo.profileImage ? (
          <UserAvatar
            source={profileInfo ? profileInfo.profileImage.secureUrl : ""}
          />
        ) : (
          <Stack
            sx={(theme) => ({
              width: "30px",
              height: "30px",
              borderRadius: "30px",
              border: `1px solid ${theme.palette.action.hover}`,
              overflow: "hidden",
              justifyContent: "center",
              alignItems: "center",
              color: theme.palette.primary.main,
            })}
          >
            <Typography variant="caption">
              {userInfo ? userInfo.name.charAt(0) : "QB"}
            </Typography>
          </Stack>
        )}
        <Stack>
          <Typography variant="body2">{userInfo && userInfo.name}</Typography>
          <Typography
            sx={(theme) => ({
              marginY: "-5px",
              fontSize: "10px",
            })}
            variant="caption"
          >
            {dayjs(comment.createdAt).format("dd, DD/MM/YYYY hh:mm a")}
          </Typography>
        </Stack>
      </Stack>
      <Divider />
      <Stack
        sx={(theme) => ({
          padding: theme.spacing(0, 2),
        })}
      >
        <Typography
          variant="caption"
          sx={(theme) => ({
            fontSize: theme.spacing(1.25),
            lineHeight: "15px",
          })}
        >
          {comment.comment}
        </Typography>
      </Stack>
    </Stack>
  );
}
