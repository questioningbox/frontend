import { SvgIconTypeMap } from "@mui/material";
import { OverridableComponent } from "@mui/material/OverridableComponent";
import React from "react";
import { IconType } from "react-icons";
import {
  RiHome3Line,
  RiQuestionnaireLine,
  RiGroupLine,
  RiAccountPinCircleLine,
} from "react-icons/ri";
import { MdOutlineLiveHelp } from "react-icons/md";
import { FiCreditCard } from "react-icons/fi";
import NavigationRoutes from "../../../routes/NavigationRoutes";
import { IHomeCategoriesLink } from "../../../interface";

export const HomeCategories: IHomeCategoriesLink[] = [
  { title: "Students", route: NavigationRoutes.home.students },
  { title: "Teachers", route: NavigationRoutes.home.teachers },
  { title: "Schools", route: "" },
  { title: "Exams", route: NavigationRoutes.preptest.root },
];
