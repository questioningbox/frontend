import { colors, Divider, Stack, Typography } from "@mui/material";
import React, { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { AlertModal, ContentLoader, SearchInput } from "../../../components";
import { TeachersThunk } from "../../../functions/slice";
import ApiRoutes from "../../../routes/ApiRoutes";
import { CustomIconButton } from "../../../shared";
import { PersonInfoCard } from "../components";

export default function TeachersPage() {
  const dispatch = useAppDispatch();
  const { user } = useAppSelector((state) => state.UserReducer);
  const { loading, error, message, teachers } = useAppSelector(
    (state) => state.TeachersReducer
  );

  function getTeachers() {
    dispatch(
      TeachersThunk({
        method: "get",
        url: ApiRoutes.home.teachers,
        params: { page: 1, pageSize: 100 },
      })
    );
  }

  useEffect(() => {
    getTeachers();
  }, []);
  return (
    <Stack
      width="100%"
      bgcolor={(theme) => theme.palette.action.hover}
      spacing={1}
      height="100vh"
    >
      <AlertModal
        isMessage={message ? true : false}
        isError={error ? true : false}
        text={error && message}
        action={
          <CustomIconButton
            size="small"
            variant="contained"
            title="Close"
            props={{
              style: {
                borderColor: message
                  ? colors.blue[500]
                  : error
                  ? colors.red[500]
                  : "inherit",
                color: message
                  ? colors.blue[500]
                  : error
                  ? colors.red[500]
                  : "inherit",
              },
            }}
          />
        }
      />
      <Stack
        direction="row"
        width="100%"
        alignItems="center"
        justifyContent="space-between"
        sx={(theme) => ({
          [theme.breakpoints.down("sm")]: {
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "flex-start",
          },
        })}
        padding={(theme) => theme.spacing(1, 2)}
        border={(theme) =>
          `1px solid ${theme.palette.action.disabledBackground}`
        }
        borderRadius={0}
        bgcolor={(theme) => theme.palette.common.white}
      >
        <Stack direction="row" alignItems="center" justifyContent="flex-start">
          <Typography variant="body1">
            Connect with your Teachers and Teachers from Other Institutions
          </Typography>
        </Stack>
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="flex-end"
          sx={(theme) => ({
            [theme.breakpoints.down("sm")]: {
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "flex-start",
              marginTop: 1,
            },
          })}
          spacing={1}
        >
          <SearchInput placeholder="find friends" />
          <CustomIconButton title="search" variant="outlined" size="small" />
        </Stack>
      </Stack>
      <Stack padding={1} width="100%" spacing={1}>
        {loading && <ContentLoader />}
        {!loading &&
          teachers.results
            .filter((u) => u.user.userId !== user?.userId)
            .filter((f) => f.profile !== null)
            .map((s) => <PersonInfoCard info={s} key={s.user._id} isStudent />)}
      </Stack>
    </Stack>
  );
}
