import { Box, Divider, Stack, useMediaQuery } from "@mui/material";
import React, { useEffect, useState } from "react";
import { Outlet } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { HomeSidebarLinks } from "../../../data/data";
import {
  CourseThunk,
  FeedsThunk,
  UserProfileThunk,
} from "../../../functions/slice";
import ApiRoutes from "../../../routes/ApiRoutes";
import { AppFooter, FeedsDrawer, Sidebar } from "../../../views";
import { Navbar } from "../components";
import SocketClient, { Socket } from "socket.io-client";
import { baseUrl } from "../../../controller";
import EventNames from "../../../routes/EventNames";
import { setFeeds } from "../../../features/slice/FeedsReducer";
import FeedModel from "../../../models/FeedModel";

////
let socket: Socket;
export default function HomePage() {
  const isMobile = useMediaQuery("(max-width:567px)");
  const { user } = useAppSelector((state) => state.UserReducer);
  const [feedsDrawer, setFeedsDrawer] = useState<boolean>(false);
  const dispatch = useAppDispatch();
  const [sidebar, setSidebar] = useState<boolean>(isMobile ? false : true);
  function handleSidebar() {
    setSidebar(!sidebar);
  }

  useEffect(() => {
    socket = SocketClient(baseUrl);
    socket.on(EventNames.feed.add, (data: FeedModel[]) => {
      dispatch(setFeeds(data));
    });

    socket.on(EventNames.feed.comment, (data: FeedModel[]) => {
      dispatch(setFeeds(data));
    });

    socket.on(EventNames.feed.delete, (data: FeedModel[]) => {
      dispatch(setFeeds(data));
    });

    dispatch(CourseThunk({ method: "get", url: ApiRoutes.course.crud }));
    ////
    if (user) {
      dispatch(
        UserProfileThunk({
          method: "get",
          url: ApiRoutes.profile.crud,
          token: user.token,
          data: null,
        })
      );
    }
  }, []);

  useEffect(() => {
    if (isMobile) {
      setSidebar(false);
    } else {
      setSidebar(true);
    }
  }, [isMobile]);

  return (
    <Stack
      sx={(theme) => ({
        overflowX: "hidden",
      })}
      className="sub-body"
    >
      <Sidebar
        routes={HomeSidebarLinks(user)}
        sidebar={sidebar}
        handleSidebar={handleSidebar}
      />

      <Stack className="sub-body" width="100%">
        <Navbar
          sidebar={sidebar}
          handleSidebar={handleSidebar}
          handleFeedsDrawer={() => setFeedsDrawer(!feedsDrawer)}
        />
        <Stack marginTop="60px" />
        <Divider />
        <Stack
          width="100%"
          alignItems="center"
          paddingLeft={sidebar ? "80px" : 0}
        >
          <Outlet />
        </Stack>
        <Stack className="footer" paddingLeft={sidebar ? "80px" : 0}>
          <AppFooter />
        </Stack>
      </Stack>
    </Stack>
  );
}
