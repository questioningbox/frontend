export { default as HomePage } from "./HomePage";
export { default as ContentPage } from "./ContentPage";
export { default as TeachersPage } from "./TeachersPage";
export { default as StudentsPage } from "./StudentsPage";
export { default as CourseDetailsPage } from "./CourseDetailsPage";
