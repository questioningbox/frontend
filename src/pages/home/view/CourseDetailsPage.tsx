import {
  Box,
  Container,
  Divider,
  Grid,
  Stack,
  Typography,
  useMediaQuery,
} from "@mui/material";
import dayjs from "dayjs";
import React, { useEffect, useState } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { ContentLoader, ProfileAvatar } from "../../../components";
import controller from "../../../controller";
import {
  responseFailed,
  responsePending,
  responseSuccessful,
} from "../../../features/slice/ResponseSlice";
import { PagedResults } from "../../../models";
import ApiResponseModel from "../../../models/ApiResponseModel";
import { CourseInfo } from "../../../models/CourseModel";
import { CustomIconButton } from "../../../shared";
import { CourseInfoCard } from "../../../views";

export default function CourseDetailsPage() {
  const dispatch = useAppDispatch();
  const navigation = useNavigate();
  const [info, setInfo] = useState<CourseInfo | null>(null);
  const [id, setId] = useState<string | null>(null);
  const { user } = useAppSelector((state) => state.UserReducer);
  const [relatedLoading, setRelatedLoading] = useState(false);
  const [subscriptionLoading, setSubscriptionLoading] = useState(false);
  const [related, setRelated] = useState<PagedResults<CourseInfo>>({
    results: [],
    page: 0,
    pageSize: 0,
    totalDocuments: 0,
    totalPages: 0,
  });
  const { loading } = useAppSelector((state) => state.ResponseReducer);
  const [searchParams] = useSearchParams();
  // const isMobile=u

  async function handleCourseSubscription() {
    try {
      setSubscriptionLoading(true);
      const res = await controller<ApiResponseModel<CourseInfo>>({
        method: "patch",
        token: user?.token,
        url: `course/subscription/${info?.course?.courseId}`,
      });
      setSubscriptionLoading(false);
      setInfo(res.data);
    } catch (error) {
      setSubscriptionLoading(false);
      dispatch(responseFailed(error));
    }
  }

  //
  async function getCourseInfo(courseId?: string) {
    try {
      dispatch(responsePending());
      const res = await controller<ApiResponseModel<CourseInfo>>({
        method: "get",
        url: !courseId ? `course/details/${id}` : `course/details/${courseId}`,
      });
      setInfo(res.data);

      dispatch(responseSuccessful(res.message));
    } catch (error) {
      dispatch(responseFailed(error));
    }
  }

  useEffect(() => {
    getRelatedCourses();
  }, [info]);

  async function getRelatedCourses() {
    try {
      if (info) {
        setRelatedLoading(true);
        const res = await controller<
          ApiResponseModel<PagedResults<CourseInfo>>
        >({
          method: "get",
          url: `course/related/${info.author.user.userId}`,
        });
        setRelated(res.data);
        setRelatedLoading(false);
      }
    } catch (error) {
      dispatch(responseFailed(error));
      setRelatedLoading(false);
    }
  }

  useEffect(() => {
    setId(searchParams.get("id"));
  }, []);

  useEffect(() => {
    if (id) {
      getCourseInfo();
    }
  }, [id]);

  return (
    <Stack spacing={1.5} padding={0} width="100%">
      {loading && <ContentLoader />}
      {!loading && info && (
        <Stack>
          <Stack
            padding={2}
            sx={(theme) => ({
              bgcolor: theme.palette.action.hover,
            })}
            width="100%"
            spacing={2}
            marginBottom={3}
          >
            <Stack width="100%">
              <Stack spacing={1} width="100%" direction="row">
                <Stack>
                  <ProfileAvatar
                    size="small"
                    profileUrl={info.course.coverImage.url}
                  />
                </Stack>
                <Stack>
                  <Stack
                    direction="row"
                    alignItems="center"
                    justifyContent="flex-start"
                    spacing={1}
                  >
                    <Typography variant="h6">{info.course.title}</Typography>
                    <Typography
                      sx={(theme) => ({
                        color: theme.palette.common.white,
                        bgcolor: theme.palette.primary.main,
                        padding: theme.spacing(0),
                        fontSize: theme.spacing(1.5),
                        borderRadius: "30px",
                        minWidth: "60px",
                        height: "20px",
                        textAlign: "center",
                        lineHeight: "20px",
                      })}
                      variant="caption"
                    >
                      {info.course.courseType === "paid"
                        ? `Ghs${info.course.fee}`
                        : "free"}
                    </Typography>
                  </Stack>
                  <Stack direction="row" spacing={1}>
                    <Typography variant="body1">Starting on:</Typography>
                    <Typography variant="body1">
                      {dayjs(info.course.duration.startDate).format(
                        "DD MMMM, YYYY"
                      )}
                    </Typography>
                    <Stack
                      alignItems="center"
                      justifyContent="flex-start"
                      direction="row"
                      spacing={1}
                    >
                      <Stack
                        width="8px"
                        height="8px"
                        borderRadius="8px"
                        bgcolor={(theme) => theme.palette.primary.main}
                      />
                      <Typography variant="body2">
                        {`${info.course.duration.duration} ${info.course.duration.period}`}
                      </Typography>
                    </Stack>
                  </Stack>
                  <Divider />

                  {/* <Divider /> */}
                </Stack>
              </Stack>
              <Typography variant="body2">{info.course.description}</Typography>
              <Stack
                padding={1}
                width="100%"
                direction="row"
                alignItems="center"
                justifyContent="flex-start"
                spacing={info.course.registeredStudents.length > 0 ? 1.5 : 0}
              >
                <Typography variant="body1">
                  {info.course.registeredStudents.length > 0 &&
                    info.course.registeredStudents.length + ` Subscribers`}
                </Typography>
                {user &&
                user.userId !== info.course.userId &&
                info.course.registeredStudents.includes(user.userId) ? (
                  <CustomIconButton
                    size="small"
                    variant="contained"
                    title={subscriptionLoading ? "loading..." : "Unsubscribe"}
                    props={{
                      disabled: subscriptionLoading,
                      style: {
                        backgroundColor: "firebrick",
                      },
                    }}
                    handleClick={handleCourseSubscription}
                  />
                ) : (
                  user &&
                  user.userId !== info.course.userId && (
                    <CustomIconButton
                      size="small"
                      variant="contained"
                      props={{ disabled: subscriptionLoading }}
                      title={subscriptionLoading ? "loading..." : "Subscribe"}
                      handleClick={handleCourseSubscription}
                    />
                  )
                )}
              </Stack>
              <Stack>
                <Typography variant="body1">
                  Subcribe to gain free access to the course materials
                </Typography>
              </Stack>
            </Stack>
            <Divider />
            <Stack width="100%">
              <Stack
                direction="row"
                alignItems="center"
                justifyContent="flex-start"
                spacing={1}
              >
                {info.author &&
                  info.author.profile &&
                  info.author.profile.profileImage && (
                    <ProfileAvatar
                      size="small"
                      profileUrl={info.author.profile.profileImage.url}
                    />
                  )}
                <Stack>
                  <Typography variant="body1">
                    {info.author.user.name}
                  </Typography>
                  {info.author.profile && (
                    <Stack>
                      <Typography variant="body2">
                        {`followers: ${info.author.profile.followers.length}`}
                      </Typography>
                    </Stack>
                  )}
                </Stack>
                <Stack flex={1} />
                <CustomIconButton
                  title="View Profile"
                  variant="outlined"
                  size="small"
                />
              </Stack>
            </Stack>
          </Stack>
          <Stack padding={2}>
            <Typography variant="h5">Related Courses</Typography>
          </Stack>
          <Divider />
          <Stack padding={2}>
            {relatedLoading && <ContentLoader />}
            {!relatedLoading && (
              <Grid
                container
                alignItems="center"
                justifyContent="center"
                spacing={2}
              >
                {related.results
                  .filter((r) => r.course._id !== info.course._id)
                  .map((c) => (
                    <CourseInfoCard
                      key={c.course._id}
                      info={c}
                      handleMore={() => {
                        setId(c.course.courseId);
                        getCourseInfo(c.course.courseId);
                        navigation(`/course?q=info&id=${c.course.courseId}`);
                      }}
                    />
                  ))}
              </Grid>
            )}
          </Stack>
        </Stack>
      )}
    </Stack>
  );
}
