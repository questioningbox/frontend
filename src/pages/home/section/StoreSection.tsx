import { CircularProgress, Grid, Stack } from "@mui/material";
import React from "react";
import { useAppSelector } from "../../../app/hooks";
import { ContentLoader, NoCoursesCard } from "../../../components";
import { getStoreItems } from "../../../utilities";
import { CourseInfoCard, StoreItem } from "../../../views";

export default function StoreSection() {
  const { courses } = useAppSelector((state) => state.CoursesReducer);
  const { loading } = useAppSelector((state) => state.ResponseReducer);
  return (
    <Stack height="100%">
      {!loading && courses && courses.results.length > 0 && (
        <Grid
          container
          alignItems="center"
          justifyContent="center"
          width="100%"
          spacing={1}
          padding={2}
        >
          {courses.results.map((item) => (
            <CourseInfoCard info={item} key={item.course._id} />
          ))}
        </Grid>
      )}
      {loading && <ContentLoader />}

      {!loading && courses && courses.results.length <= 0 && <NoCoursesCard />}
    </Stack>
  );
}
