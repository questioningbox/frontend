import { Stack } from "@mui/material";
import React from "react";
import { Outlet } from "react-router-dom";
import { AppFooter } from "../../../views";

export default function RootPage() {
  return (
    <Stack
      height="100%"
      width="100%"
      sx={(theme) => ({
        overflowY: "auto",
      })}
    >
      <Stack
        sx={(theme) => ({
          overflowY: "auto",
        })}
        height="100vh"
        width="100%"
      >
        <Outlet />
      </Stack>
    </Stack>
  );
}
