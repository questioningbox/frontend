import { Stack, Typography } from "@mui/material";
import React from "react";
import { OutlineLinkButton } from "../../../components";

export default function ContentHeader() {
  return (
    <Stack padding={2} alignItems="center" width="100%" justifyContent="center">
      <Typography variant="body2" color="primary">
        Pricing
      </Typography>
      <Typography variant="h5">Simple, transparent pricing</Typography>
      <Typography variant="caption">
        we believe Quizbox should be accessible to all companies no matter the
        size
      </Typography>
      <Stack
        width="100%"
        alignItems="center"
        justifyContent="center"
        paddingY={1.5}
        spacing={1}
        direction="row"
      >
        <OutlineLinkButton text="Monthly billing" />
        <OutlineLinkButton text="Annual billing" />
      </Stack>
    </Stack>
  );
}
