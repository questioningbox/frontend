import { Divider, Grid, Stack } from "@mui/material";
import React, { useEffect, useState } from "react";
import { BsCloudPlus } from "react-icons/bs";
import { GrFormAdd } from "react-icons/gr";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { ContentLoader, SearchInput } from "../../../components";
import controller from "../../../controller";
import {
  responseFailed,
  responsePending,
  responseSuccessful,
} from "../../../features/slice/ResponseSlice";
import { PagedResults } from "../../../models";
import ApiResponseModel from "../../../models/ApiResponseModel";
import { CourseModel } from "../../../models/CourseModel";
import { CustomIconButton } from "../../../shared";
import { StoreItem } from "../../../views";
import { AddCourseModal } from "../components";

export default function CoursesTab() {
  const [addCourse, setAddCourse] = useState<boolean>(false);
  const dispatch = useAppDispatch();
  const { loading } = useAppSelector((state) => state.ResponseReducer);
  const { user } = useAppSelector((state) => state.UserReducer);
  const [courses, setCourses] = useState<PagedResults<CourseModel>>({
    results: [],
    page: 1,
    pageSize: 10,
    totalPages: 0,
    totalDocuments: 0,
  });

  //
  async function getCourses() {
    try {
      dispatch(responsePending());
      const res = await controller<ApiResponseModel<PagedResults<CourseModel>>>(
        {
          method: "get",
          url: "course/user",
          token: user?.token || "",
          params: { page: 1, pageSize: 100 },
        }
      );
      dispatch(responseSuccessful(res.message));
      setCourses(res.data);
    } catch (error: any) {
      dispatch(responseFailed(error?.message || error));
    }
  }

  useEffect(() => {
    getCourses();
  }, []);
  return (
    <Stack spacing={1} width="100%" height="100%">
      <Stack
        direction="row"
        width="100%"
        alignItems="center"
        justifyContent="flex-end"
        spacing={1.5}
        padding={1.5}
      >
        <CustomIconButton
          handleClick={getCourses}
          size="small"
          title="Refresh"
          variant="outlined"
        />
        <Stack flex={1} />
        <AddCourseModal
          open={addCourse}
          handleClose={() => setAddCourse(false)}
        />
        <SearchInput placeholder="search course..." />
        <CustomIconButton
          size="small"
          variant="contained"
          title="Add Course"
          Icon={BsCloudPlus}
          handleClick={() => setAddCourse(true)}
        />
      </Stack>
      <Divider />
      <Stack width="100%">
        {loading && <ContentLoader />}
        {!loading && courses && (
          <Grid
            container
            spacing={2}
            alignItems="center"
            justifyContent="center"
          >
            {courses.results.map((c) => (
              <StoreItem key={c._id} info={c} />
            ))}
          </Grid>
        )}
      </Stack>
    </Stack>
  );
}
