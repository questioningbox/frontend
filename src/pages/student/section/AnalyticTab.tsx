import { Grid, Stack, Typography, useMediaQuery } from "@mui/material";
import React, { useEffect } from "react";
import { generateId } from "../../../services";
import { AnalyticItem } from "../components";
import { CgPentagonUp } from "react-icons/cg";
import { GiTrophyCup } from "react-icons/gi";
import { TbSend, TbUsers } from "react-icons/tb";
import { BsBook } from "react-icons/bs";
import { PieChart } from "react-minimal-pie-chart";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { UserAnalyticsThunk } from "../../../functions/slice";
import { ContentLoader } from "../../../components";
export default function AnalyticTab() {
  const isMobile = useMediaQuery("(max-width:567px)");
  const { user, analytics } = useAppSelector((state) => state.UserReducer);
  const { loading } = useAppSelector((state) => state.ResponseReducer);
  const dispatch = useAppDispatch();

  useEffect(() => {
    console.log("user analytics ", analytics);
    if (user && user.token) {
      dispatch(
        UserAnalyticsThunk({
          token: user.token,
          method: "get",
          url: "user/analytics",
        })
      );
    }
  }, []);
  return (
    <Stack
      width="100%"
      direction={isMobile ? "column" : "row"}
      alignItems="flex-start"
      justifyContent="center"
      sx={(theme) => ({
        [theme.breakpoints.down("sm")]: {
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "flex-start",
        },
      })}
      paddingBottom={3}
    >
      <Stack paddingY={2} flex={1}>
        {loading && <ContentLoader />}
        {!loading && analytics && (
          <Grid
            columns={2}
            container
            spacing={2}
            justifyContent="center"
            alignItems="center"
            gridColumn={2}
          >
            <AnalyticItem
              Icon={<CgPentagonUp size={40} color="orange" />}
              label="Competitions"
              value={analytics.competitions}
            />
            <AnalyticItem
              Icon={<GiTrophyCup color="seagreen" size={25} />}
              value={analytics.competitionsWon}
              label="Total wins"
            />
            <AnalyticItem
              Icon={<TbUsers size={20} color="midnightblue" />}
              label="Following"
              value={analytics.following}
            />

            <AnalyticItem
              Icon={<TbUsers size={20} color="midnightblue" />}
              label="Followers"
              value={analytics.followers}
            />

            <AnalyticItem
              Icon={<TbSend size={20} color="firebrick" />}
              label="All posts"
              value={analytics.posts}
            />
            <AnalyticItem
              Icon={<BsBook size={20} color="green" />}
              label="Prep tests"
              value={analytics.prepTest}
            />
          </Grid>
        )}
      </Stack>
      <Stack
        padding={isMobile ? 1 : 3}
        alignItems="center"
        justifyContent="center"
        flex={1}
      >
        <Stack
          padding={2}
          border={(theme) =>
            `1px solid ${theme.palette.action.disabledBackground}`
          }
          height="80%"
          width="100%"
          alignItems="center"
          justifyContent="center"
          spacing={2}
        >
          <Typography
            sx={(theme) => ({
              width: "100%",
              textAlign: "left",
            })}
            variant="body1"
            fontWeight="bold"
          >
            Subjects Analytics
          </Typography>
          <PieChart
            animate
            animationDuration={500}
            startAngle={-180}
            labelPosition={50}
            labelStyle={{ borderRadius: "5px" }}
            style={{ width: "250px", height: "250px" }}
            data={[
              { title: "Subject 1", value: 77, color: "#F8C6AB" },
              { title: "Subject 2", value: 55, color: "#FEA21B" },
              { title: "Subject 3", value: 33, color: "#29ACD6" },
              { title: "Subject 4", value: 45, color: "#F42C2C" },
            ]}
          />
        </Stack>
      </Stack>
    </Stack>
  );
}
