import React, { ChangeEvent, useEffect, useState } from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import Slide from "@mui/material/Slide";
import { TransitionProps } from "@mui/material/transitions";
import {
  Divider,
  IconButton,
  InputAdornment,
  MenuItem,
  Stack,
  Typography,
} from "@mui/material";
import { MdOutlineClose } from "react-icons/md";
import {
  CustomDatePicker,
  InputGroup,
  PrimaryIconButton,
} from "../../../components";
import { AiOutlineCloudUpload } from "react-icons/ai";
import { CourseDto } from "../../../models/CourseModel";
import { CoursePeriod, CourseType } from "../../../models";
import dayjs from "dayjs";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import {
  responseFailed,
  responsePending,
  responseSuccessful,
} from "../../../features/slice/ResponseSlice";
import controller from "../../../controller";
import ApiResponseModel from "../../../models/ApiResponseModel";
import ApiRoutes from "../../../routes/ApiRoutes";
import { DialogHeader } from "../../../shared";

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

interface IProps {
  open: boolean;
  handleClose: () => void;
}

export default function AddCourseModal({ open, handleClose }: IProps) {
  const { loading } = useAppSelector((state) => state.ResponseReducer);
  const dispatch = useAppDispatch();
  const { user } = useAppSelector((state) => state.UserReducer);
  const [coverImage, setCoverImage] = useState<File | null>(null);
  const [coverPreview, setCoverPreview] = useState<any>(null);
  const [info, setInfo] = useState<CourseDto>({
    courseType: CourseType.Free,
    description: "",
    title: "",
    fee: 0,
    duration: {
      startDate: dayjs().format(),
      period: CoursePeriod.Weeks,
      duration: 1,
    },
  });

  async function handleUpload() {
    const formData = new FormData();
    formData.append("file", coverImage as any);
    formData.append("fee", info.fee as any);
    formData.append("period", info.duration.period);
    formData.append("duration", info.duration.duration as any);
    formData.append("startDate", info.duration.startDate);
    formData.append("description", info.description);
    formData.append("title", info.title);
    formData.append("courseType", info.courseType as any);

    try {
      dispatch(responsePending());
      const data = await controller<ApiResponseModel<any>>({
        data: formData,
        isFile: true,
        token: user?.token,
        url: ApiRoutes.course.crud,
        method: "post",
      });
      dispatch(responseSuccessful(data.message));
    } catch (error) {
      dispatch(responseFailed(error));
    }
  }

  useEffect(() => {
    if (coverImage) {
      const fileReader = new FileReader();
      fileReader.readAsDataURL(coverImage);
      fileReader.addEventListener("load", (e) => {
        setCoverPreview(e.target?.result);
      });
    }
  }, [coverImage]);
  return (
    <Dialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      aria-describedby="alert-dialog-slide-description"
      fullWidth
      maxWidth="sm"
    >
      <DialogTitle>
        <DialogHeader title="Upload Course" handleClose={handleClose} />
      </DialogTitle>
      <DialogContent dividers>
        {coverPreview && (
          <Stack height="250px" width="100%">
            <img src={coverPreview} alt="cover-preview-image" />
          </Stack>
        )}
        <Stack spacing={1}>
          <InputGroup
            handleChange={(e) => setInfo({ ...info, title: e.target.value })}
            placeholder="enter course title"
            label="Course Title"
          />
          <InputGroup
            handleChange={(e) =>
              setInfo({ ...info, courseType: e.target.value as any })
            }
            placeholder="Choose Course Type"
            props={{ select: true, defaultValue: "free" }}
            label="Course Type"
          >
            {[
              { title: "Free", value: "free" },
              { title: "Paid", value: "paid" },
            ].map((ct) => (
              <MenuItem value={ct.value} key={ct.value}>
                {ct.title}
              </MenuItem>
            ))}
          </InputGroup>
          <InputGroup
            placeholder="Course Description"
            label="Course Description"
            props={{ multiline: true }}
            handleChange={(e) =>
              setInfo({ ...info, description: e.target.value })
            }
          />

          <Typography variant="body2">Course Duration</Typography>
          <Divider />
          <Stack
            direction="row"
            alignItems="center"
            justifyContent="center"
            width="100%"
            spacing={1}
            marginY={1.5}
          >
            <InputGroup
              label="Period"
              props={{ select: true, defaultValue: "days" }}
              handleChange={(e) =>
                setInfo({
                  ...info,
                  duration: { ...info.duration, period: e.target.value as any },
                })
              }
            >
              {[
                { title: "Days", value: "days" },
                { title: "Weeks", value: "weeks" },
                { title: "Months", value: "months" },
                { title: "Years", value: "years" },
              ].map((p) => (
                <MenuItem value={p.value} key={p.value}>
                  {p.title}
                </MenuItem>
              ))}
            </InputGroup>
            <InputGroup
              props={{ type: "number" }}
              placeholder="duration"
              label="Duration"
              handleChange={(e) =>
                setInfo({
                  ...info,
                  duration: {
                    ...info.duration,
                    duration: parseInt(e.target.value),
                  },
                })
              }
            />
            <Stack
              alignItems="center"
              paddingTop={3.5}
              justifyContent="center"
              height="100%"
            >
              <CustomDatePicker
                handleChange={(val) =>
                  setInfo({
                    ...info,
                    duration: { ...info.duration, startDate: val },
                  })
                }
                placeholder="Start Date"
              />
            </Stack>
          </Stack>

          <InputGroup
            props={{
              InputProps: {
                startAdornment: (
                  <InputAdornment position="start">
                    <Typography variant="caption">$</Typography>
                  </InputAdornment>
                ),
              },
              type: "number",
            }}
            label="Price"
            handleChange={(e) => {
              if (!isNaN(parseFloat(e.target.value))) {
                setInfo({ ...info, fee: parseFloat(e.target.value) });
              }
            }}
            placeholder="course price"
          />
          <input
            id="cover-file-input"
            style={{ display: "none" }}
            type="file"
            placeholder="course cover image"
            multiple={false}
            accept="image/*.jpg|*.png"
            onChange={(e: ChangeEvent<HTMLInputElement>) => {
              console.log(e.target.files);
              if (e.target.files && e.target.files.length > 0) {
                setCoverImage(e.target.files[0]);
              } else {
                setCoverImage(null);
                setCoverPreview(null);
              }
            }}
          />
          <Typography
            component="label"
            color="primary"
            htmlFor="cover-file-input"
            sx={(theme) => ({
              padding: theme.spacing(1),
              width: "100%",
              textAlign: "center",
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
            })}
          >
            Choose Cover Image
            <AiOutlineCloudUpload style={{ marginLeft: "20px" }} />
          </Typography>
        </Stack>
      </DialogContent>
      <DialogActions>
        <PrimaryIconButton
          handleClick={handleUpload}
          Icon={AiOutlineCloudUpload}
          title="Upload"
          props={{ disabled: loading }}
        />
      </DialogActions>
    </Dialog>
  );
}
