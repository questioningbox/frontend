import { Menu, MenuItem } from "@mui/material";
import React from "react";

interface IProps {
  anchorEl: HTMLElement | null;
  handleClose: () => void;
}
export default function ProfileSettingsMenu({ anchorEl, handleClose }: IProps) {
  return (
    <Menu open={Boolean(anchorEl)} anchorEl={anchorEl} onClose={handleClose}>
      <MenuItem onClick={handleClose}>Upgrade to Premium</MenuItem>
      <MenuItem onClick={handleClose}>Add Pictures</MenuItem>
    </Menu>
  );
}
