import { Grid, Stack, SvgIconTypeMap, Typography } from "@mui/material";
import { OverridableComponent } from "@mui/material/OverridableComponent";
import React, { ReactNode } from "react";
import { IconType } from "react-icons";

interface IProps {
  Icon: ReactNode;
  label: string;
  value: any;
}
export default function AnalyticItem({ Icon, label, value }: IProps) {
  return (
    <Grid flex={1} item>
      <Stack
        padding={1}
        borderRadius={1}
        border={(theme) =>
          `1px solid ${theme.palette.action.disabledBackground}`
        }
        sx={(t) => ({
          transition: "all 0.45s ease-in-out",
          "&:hover": {
            boxShadow: t.shadows[1],
          },
        })}
        alignItems="center"
        justifyContent="center"
      >
        <Stack alignItems="center" justifyContent="center" direction="row">
          {Icon}
          <Stack marginX={1.5} />
          <Stack>
            <Typography variant="caption">{label}</Typography>
            <Typography variant="body1" fontWeight="bold">
              {value}
            </Typography>
          </Stack>
        </Stack>
      </Stack>
    </Grid>
  );
}
