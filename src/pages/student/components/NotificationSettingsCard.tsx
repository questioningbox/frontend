import { Stack, Switch, Typography } from "@mui/material";
import React from "react";

interface IProps {
  subtitle?: string;
  label: string;
  handleClick?: () => void;
}
export default function NotificationSettingsCard({
  handleClick,
  label,
  subtitle,
}: IProps) {
  return (
    <Stack direction="row" alignItems="center" justifyContent="space-between">
      <Typography variant="body2">{label}</Typography>
      <Switch
        size="small"
        color="info"
        onClick={handleClick}
        checked
        value={1}
      />
    </Stack>
  );
}
