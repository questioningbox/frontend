import {
  Button,
  Divider,
  Grid,
  IconButton,
  Stack,
  Tab,
  Tabs,
  useMediaQuery,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { AiOutlineMore } from "react-icons/ai";
import { useAppSelector } from "../../../app/hooks";
import { UserReducer } from "../../../features/auth";
import ProfileModel from "../../../models/ProfileModel";
import { ProfileViewModal } from "../../../shared";
import {
  GalleryItem,
  ProfileCard,
  ProfileHeader,
  ProfileMenu,
  ProfileSettingsMenu,
} from "../components";
import { AnalyticTab, CoursesTab, UserFriendsTab } from "../section";
import { getFakeStudent, getGalleryImages, IFakeStudent } from "../services";
export default function StudentProfilePage() {
  const { user } = useAppSelector((state) => state.UserReducer);
  const [profileMenu, setProfileMenu] = useState<HTMLElement | null>(null);
  const [gallery, setGallery] = useState<string[]>([]);
  const [userProfile, setUserProfile] = useState<ProfileModel | null>(null);
  const [tab, setTab] = useState<number>(0);
  const [viewProfile, setViewProfile] = useState<boolean>(false);
  const [settingsMenu, setSettingsMenu] = useState<HTMLElement | null>(null);
  const isMobile = useMediaQuery("(max-width:567px)");
  useEffect(() => {
    setGallery(getGalleryImages(Math.floor(Math.random() * 25)));
  }, []);

  useEffect(() => {
    setGallery(getGalleryImages(Math.floor(Math.random() * 25)));
  }, [tab]);
  return (
    <Stack
      padding={1}
      width="100%"
      sx={(theme) => ({
        [theme.breakpoints.up("md")]: {
          padding: theme.spacing(0, 6),
        },
      })}
    >
      {userProfile && (
        <ProfileViewModal
          open={Boolean(userProfile || viewProfile)}
          handleClose={() => {
            setViewProfile(false);
            setUserProfile(null);
          }}
          profile={userProfile}
        />
      )}

      <ProfileMenu
        anchorEl={profileMenu}
        handleClose={() => setProfileMenu(null)}
      />
      <ProfileSettingsMenu
        anchorEl={settingsMenu}
        handleClose={() => setSettingsMenu(null)}
      />
      {user ? (
        <ProfileHeader
          pageTitle="My Account"
          handleProfileMenu={(e) => setProfileMenu(e.currentTarget)}
        />
      ) : null}
      <Stack marginY={1} />
      <Divider />
      {user ? <ProfileCard /> : null}
      <Stack
        direction="row"
        width="100%"
        alignItems="center"
        justifyContent="space-between"
        paddingTop={1.5}
      >
        <Stack
          direction="row"
          alignItems="center"
          flex={1}
          justifyContent="flex-start"
        >
          <Tabs
            onChange={(e, newValue) => setTab(parseInt(newValue))}
            value={`${tab}`}
            aria-label="basic tabs example"
          >
            <Tab
              sx={(theme) => ({
                textTransform: "none",
                fontSize: theme.spacing(1.6),
                color: tab === 0 ? theme.palette.primary.main : "inherit",
              })}
              value="0"
              label="Friends"
            />

            <Tab
              sx={(theme) => ({
                textTransform: "none",
                fontSize: theme.spacing(1.6),
                color: tab === 1 ? theme.palette.primary.main : "inherit",
              })}
              value="1"
              label="Analytics"
            />

            {user && user.userType !== "student" && (
              <Tab
                sx={(theme) => ({
                  textTransform: "none",
                  fontSize: theme.spacing(1.6),
                  color: tab === 2 ? theme.palette.primary.main : "inherit",
                })}
                value="2"
                label="Courses"
              />
            )}
          </Tabs>
        </Stack>
        <Stack
          direction="row"
          alignItems="center"
          flex={1}
          justifyContent="flex-end"
          spacing={1}
        >
          {!isMobile ? (
            <React.Fragment>
              <Button
                sx={(theme) => ({
                  textTransform: "none",
                  fontSize: theme.spacing(1.5),
                })}
                variant="outlined"
                size="small"
                color="inherit"
              >
                Upgrade to Premium
              </Button>
            </React.Fragment>
          ) : (
            <IconButton onClick={(e) => setSettingsMenu(e.currentTarget)}>
              <AiOutlineMore />
            </IconButton>
          )}
        </Stack>
      </Stack>
      <Divider />
      {tab === 0 && (
        <UserFriendsTab handleProfile={(up) => setUserProfile(up)} />
      )}
      {tab === 1 && <AnalyticTab />}

      {tab === 2 && <CoursesTab />}
    </Stack>
  );
}
