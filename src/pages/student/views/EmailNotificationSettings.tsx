import {
  Button,
  Chip,
  IconButton,
  MenuItem,
  Stack,
  Typography,
  useMediaQuery,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import {
  AccountSettings,
  NotificationSettingsCard,
  ProfileHeader,
  ProfileMenu,
  ProfileSettingsMenu,
} from "../components";
import { getFakeStudent, IFakeStudent } from "../services";
import { HiOutlineHome } from "react-icons/hi";
import { ProfileInfoCard, TextLink } from "../../../components";
import { CiUser } from "react-icons/ci";
import { HiOutlineEnvelope } from "react-icons/hi2";
import { AiOutlineClose, AiOutlineMore } from "react-icons/ai";
import { MdOutlineMoreVert } from "react-icons/md";
import NavigationRoutes from "../../../routes/NavigationRoutes";
import { useLocation, useNavigate } from "react-router-dom";
import { AppColors } from "../../../constants";
import { CustomModal } from "../../../shared";
import { useAppSelector } from "../../../app/hooks";
export default function EmailNotificationSettings() {
  const { user } = useAppSelector((state) => state.UserReducer);
  const navigation = useNavigate();
  const [accountSettings, setAccountSettings] = useState<HTMLElement | null>(
    null
  );
  const path = useLocation();
  const [deleteAccount, setDeleteAccount] = useState(false);
  const [profileSettingsMenu, setProfileSettingsMenu] =
    useState<HTMLElement | null>(null);
  const isMobile = useMediaQuery("(max-width:567px)");

  return (
    <Stack
      padding={(theme) => theme.spacing(2, 4)}
      width="100%"
      height="100%"
      sx={(theme) => ({
        overflowX: "hidden",
        overflowY: "auto",
      })}
    >
      <CustomModal
        props={{
          open: deleteAccount,
          maxWidth: "sm",
          sx: (theme) => ({
            height: "250px",
          }),
        }}
        Header={
          <Stack
            direction="row"
            alignItems="center"
            justifyContent="space-between"
            width="100%"
            overflow="hidden"
          >
            <Typography fontWeight="bold" variant="body1">
              Delete Account
            </Typography>

            <IconButton onClick={() => setDeleteAccount(false)} size="small">
              <AiOutlineClose />
            </IconButton>
          </Stack>
        }
        DialogAction={
          <Stack
            width="100%"
            alignItems="center"
            justifyContent="flex-end"
            spacing={1.5}
            direction="row"
            paddingX={2}
          >
            <Button
              sx={(theme) => ({
                textTransform: "none",
              })}
              variant="outlined"
              size="small"
              color="inherit"
            >
              No,keep it
            </Button>
            <Button
              sx={(theme) => ({
                textTransform: "none",
              })}
              variant="contained"
              size="small"
              color="error"
            >
              Yes Delete
            </Button>
          </Stack>
        }
        open={deleteAccount}
      >
        <Stack spacing={1}>
          <Typography variant="body2">
            Are you sure you wnat to delete your account? this action cannot be
            undone
          </Typography>
        </Stack>
      </CustomModal>
      <ProfileMenu
        anchorEl={profileSettingsMenu}
        handleClose={() => setProfileSettingsMenu(null)}
      />
      <AccountSettings
        handleDelete={() => setDeleteAccount(true)}
        anchorEl={accountSettings}
        handleClose={() => setAccountSettings(null)}
      />
      {user ? (
        <ProfileHeader
          handleProfileMenu={(e) => setProfileSettingsMenu(e.currentTarget)}
        />
      ) : null}

      <Stack alignItems="center" justifyContent="flex-start" spacing={1}>
        <Stack
          width={isMobile ? "100%" : "600px"}
          padding={2}
          alignItems="flex-start"
          justifyContent="center"
        >
          <Stack
            alignItems="center"
            justifyContent="center"
            spacing={2}
            direction="row"
          >
            <Stack
              width="60px"
              height="60px"
              borderRadius="60px"
              overflow="hidden"
              justifyContent="center"
              alignItems="center"
              border={(theme) => `1px solid ${theme.palette.action.hover}`}
              bgcolor={(theme) => theme.palette.action.hover}
            >
              <Typography variant="h4" color="primary">
                {user && user.name.charAt(0).toUpperCase()}
              </Typography>
            </Stack>
            <Stack flex={isMobile ? 1 : undefined}>
              <Stack
                alignItems="center"
                justifyContent={"flex-start"}
                direction="row"
                spacing={1}
              >
                <HiOutlineHome />
                <Typography variant="caption">General</Typography>
              </Stack>
              <Typography variant="body1" fontWeight="bold">
                {user?.name}
              </Typography>
              {user ? (
                <TextLink
                  props={{
                    sx: (theme) => ({
                      textDecoration: "none",
                      color: theme.palette.common.black,
                      fontSize: theme.spacing(1.5),
                    }),
                  }}
                  text={user ? user.username : ""}
                />
              ) : null}
            </Stack>
            {isMobile ? (
              <IconButton
                onClick={(e) => setAccountSettings(e.currentTarget)}
                size="small"
              >
                <MdOutlineMoreVert />
              </IconButton>
            ) : null}
          </Stack>
        </Stack>
        <Stack
          direction={isMobile ? "column" : "row"}
          padding={2}
          paddingTop={1}
          alignItems="center"
          justifyContent="center"
          width={isMobile ? "100%" : "600px"}
          spacing={1}
        >
          {isMobile ? null : (
            <Stack
              paddingRight={1}
              borderRight={(theme) =>
                `${isMobile ? "0" : "1"} solid ${theme.palette.action.hover}`
              }
              spacing={0.5}
            >
              <MenuItem
                sx={(theme) => ({
                  height: "30px",
                  fontSize: theme.spacing(1.45),
                  color:
                    path.pathname === NavigationRoutes.student.general
                      ? AppColors.primary
                      : "inherit",
                })}
                onClick={() => {
                  navigation(NavigationRoutes.student.general);
                }}
              >
                General
              </MenuItem>

              <MenuItem
                sx={(theme) => ({
                  height: "30px",
                  fontSize: theme.spacing(1.45),
                  color:
                    path.pathname === NavigationRoutes.student.notifications
                      ? AppColors.primary
                      : "inherit",
                })}
                onClick={() => {
                  navigation(NavigationRoutes.student.notifications);
                }}
              >
                Email Notification
              </MenuItem>
            </Stack>
          )}
          {user ? (
            <Stack
              padding={(theme) => theme.spacing(0, 2)}
              width={isMobile ? "100%" : "400px"}
              spacing={1}
            >
              <Stack>
                <Typography variant="body1" fontWeight="bold">
                  Account Activity
                </Typography>
                <Typography variant="caption">Email me when!</Typography>
              </Stack>
              <NotificationSettingsCard label="Someone invite me to challenge" />
              <NotificationSettingsCard label="Someone mention me in a comment" />
              <NotificationSettingsCard label="New features are released" />
              <NotificationSettingsCard label="Anyone follows me" />
            </Stack>
          ) : null}
        </Stack>
      </Stack>
    </Stack>
  );
}
