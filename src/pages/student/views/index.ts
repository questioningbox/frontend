export { default as StudentPage } from "./StudentPage";
export { default as StudentProfileGeneralPage } from "./StudentProfileGeneralPage";
export { default as StudentProfileEditPage } from "./StudentProfileEditPage";
export { default as StudentProfilePage } from "./StudentProfilePage";
export { default as EmailNotificationSettingsPage } from "./EmailNotificationSettings";
