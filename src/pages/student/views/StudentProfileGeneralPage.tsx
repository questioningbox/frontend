import {
  Button,
  Chip,
  Divider,
  IconButton,
  MenuItem,
  Paper,
  Stack,
  TextField,
  Typography,
  useMediaQuery,
} from "@mui/material";
import React, { ChangeEvent, useEffect, useState } from "react";
import {
  AccountSettings,
  EducationalInfoCard,
  ProfileHeader,
  ProfileMenu,
  ProfileSettingsMenu,
  WorkExperienceInfoCard,
} from "../components";
import { HiOutlineHome } from "react-icons/hi";
import {
  CustomIconButton,
  CustomInkButton,
  InputGroup,
  ProfileInfoCard,
  TextLink,
} from "../../../components";
import { CiEdit, CiPhone, CiUser } from "react-icons/ci";
import { HiOutlineEnvelope } from "react-icons/hi2";
import { AiOutlineClose, AiOutlineCloudUpload } from "react-icons/ai";
import { MdOutlineMoreVert } from "react-icons/md";
import NavigationRoutes from "../../../routes/NavigationRoutes";
import { useLocation, useNavigate } from "react-router-dom";
import {
  CustomModal,
  ProfileViewModal,
  WorkExperienceInfoModal,
} from "../../../shared";
import { AppColors } from "../../../constants";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { FiCamera, FiChevronDown } from "react-icons/fi";
import { IoMdAddCircleOutline } from "react-icons/io";
import { UserProfileThunk } from "../../../functions/slice";
import ApiRoutes from "../../../routes/ApiRoutes";
import ProfileModel, { ProfileDto } from "../../../models/ProfileModel";
import EducationalInfoModal from "../../../shared/EducationalInfoModal";
import { FaUserCog } from "react-icons/fa";
import { EducationalInfoModel, WorkExperienceInfoModel } from "../../../models";
import { BsInfo } from "react-icons/bs";
export default function StudentProfileGeneralPage() {
  const [addEducation, setAddEducation] = useState<boolean>(false);
  const [addWork, setAddWork] = useState<boolean>(false);
  const [work, setWork] = useState<WorkExperienceInfoModel | null>(null);
  const [school, setSchool] = useState<EducationalInfoModel | null>(null);
  const [profileInfo, setProfileInfo] = useState<ProfileDto>({
    address: "",
    workExperience: [],
    educationalInfo: [],
    socialLinks: [],
  });
  const { user, profile } = useAppSelector((state) => state.UserReducer);
  const navigation = useNavigate();
  const path = useLocation();
  const dispatch = useAppDispatch();
  const [profilePicturePreview, setProfilePicturePreview] = useState<any>(null);
  const [deleteAccount, setDeleteAccount] = useState<boolean>(false);
  const [accountSettings, setAccountSettings] = useState<HTMLElement | null>(
    null
  );
  const [profileImage, setProfileImage] = useState<File | null>(null);
  const [profileSettingsMenu, setProfileSettingsMenu] =
    useState<HTMLElement | null>(null);
  const isMobile = useMediaQuery("(max-width:567px)");

  function handleProfilePictureUpload() {
    const formData = new FormData();
    if (profileImage) {
      formData.append("file", profileImage);
      dispatch(
        UserProfileThunk({
          method: "post",
          token: user?.token,
          url: ApiRoutes.profile.updatePicture,
          data: formData,
          isFile: true,
        })
      );
    }
  }

  useEffect(() => {
    if (profileImage) {
      const fileReader = new FileReader();
      fileReader.readAsDataURL(profileImage);
      fileReader.addEventListener("load", (e) => {
        setProfilePicturePreview(e.target?.result);
      });
    }
  }, [profileImage]);

  useEffect(() => {
    if (profile) {
      setProfileInfo({
        ...profileInfo,
        address: profile.address,
        workExperience: profile.workExperience,
        educationalInfo: profile.educationalInfo,
        socialLinks: profile.socialLinks,
      });
    }
  }, []);

  return (
    <Stack
      padding={(theme) => theme.spacing(2, 6)}
      width="100%"
      height="100%"
      sx={(theme) => ({
        overflowX: "hidden",
        overflowY: "auto",
        paddingBottom: "150px",
        [theme.breakpoints.down("sm")]: {
          padding: theme.spacing(2, 0.5),
          paddingBottom: "150px",
        },
      })}
    >
      <ProfileMenu
        anchorEl={profileSettingsMenu}
        handleClose={() => setProfileSettingsMenu(null)}
      />
      <AccountSettings
        handleDelete={() => setDeleteAccount(true)}
        anchorEl={accountSettings}
        handleClose={() => setAccountSettings(null)}
      />
      {user ? (
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="center"
          sx={(theme) => ({
            [theme.breakpoints.down("sm")]: {
              paddingX: theme.spacing(3),
            },
          })}
        >
          <ProfileHeader
            pageTitle="Profile Details"
            handleProfileMenu={(e) => setProfileSettingsMenu(e.currentTarget)}
          />
          {isMobile ? (
            <IconButton
              onClick={(e) => setAccountSettings(e.currentTarget)}
              size="small"
            >
              <FaUserCog />
            </IconButton>
          ) : null}
        </Stack>
      ) : null}

      <Stack alignItems="center" justifyContent="flex-start" spacing={1}>
        <Stack
          width={isMobile ? "100%" : "600px"}
          padding={2}
          alignItems="flex-start"
          justifyContent="center"
        >
          <Stack
            alignItems="center"
            justifyContent="center"
            spacing={2}
            direction="row"
          ></Stack>
        </Stack>
        <Stack
          direction={isMobile ? "column" : "row"}
          padding={2}
          paddingTop={1}
          alignItems="center"
          justifyContent="center"
          width={isMobile ? "100%" : "600px"}
          spacing={1}
        >
          {isMobile ? null : (
            <Stack
              paddingRight={1}
              borderRight={(theme) =>
                `${isMobile ? "0" : "1"} solid ${theme.palette.action.hover}`
              }
              spacing={0.5}
            >
              <MenuItem
                sx={(theme) => ({
                  height: "30px",
                  fontSize: theme.spacing(1.45),
                  color:
                    path.pathname === NavigationRoutes.student.general
                      ? AppColors.primary
                      : "inherit",
                })}
                onClick={() => {
                  navigation(NavigationRoutes.student.general);
                }}
              >
                General
              </MenuItem>

              <MenuItem
                sx={(theme) => ({
                  height: "30px",
                  fontSize: theme.spacing(1.45),
                  color:
                    path.pathname === NavigationRoutes.student.notifications
                      ? AppColors.primary
                      : "inherit",
                })}
                onClick={() => {
                  navigation(NavigationRoutes.student.notifications);
                }}
              >
                Email Notification
              </MenuItem>
            </Stack>
          )}
          {user ? (
            <Stack
              padding={(theme) => theme.spacing(0, 2)}
              width={isMobile ? "100%" : "400px"}
              spacing={1}
            >
              <ProfileInfoCard
                label="FullName"
                value={user.name}
                subtitle="this name will be showed on your profile"
                Icon={<CiUser />}
              />
              {user.email && (
                <ProfileInfoCard
                  value={user.email}
                  label="Email"
                  Icon={<HiOutlineEnvelope />}
                />
              )}
              {user.phoneNumber && (
                <ProfileInfoCard
                  value={user.phoneNumber}
                  label="PhoneNumber"
                  Icon={<CiPhone />}
                />
              )}
              <Stack>
                <TextLink text="Upgrade to premium" />
                <Typography variant="caption">
                  upgrade to get access to all features now
                </Typography>
              </Stack>
            </Stack>
          ) : null}
        </Stack>
      </Stack>
      <Divider />

      <Stack marginTop={3} />
      <Stack alignItems="center">
        <Stack position="relative">
          <Stack
            width="200px"
            height="200px"
            borderRadius="200px"
            border={(theme) => `1px solid ${theme.palette.action.hover}`}
            sx={(theme) => ({
              [theme.breakpoints.down("sm")]: {
                width: "150px",
                height: "150px",
                borderRadius: "150px",
              },
            })}
            bgcolor={(theme) => theme.palette.action.hover}
            justifyContent="center"
            alignItems="center"
            overflow="hidden"
            position="relative"
          >
            <img
              src={
                profilePicturePreview
                  ? profilePicturePreview
                  : profile
                  ? profile.profileImage.secureUrl
                  : ""
              }
              alt="ppp"
              className="img"
            />
          </Stack>
        </Stack>

        <Stack width="100%"></Stack>
        <Divider />
        <Stack width="100%" spacing={1} padding={2}>
          <Typography variant="body2">Address</Typography>
          <Typography
            sx={(theme) => ({
              border: `1px solid ${theme.palette.action.disabled}`,
              padding: theme.spacing(1),
              borderRadius: theme.spacing(0.5),
            })}
            variant="body1"
          >
            {profileInfo.address}
          </Typography>
          <Stack
            spacing={1}
            component={Paper}
            elevation={2}
            width="100%"
            padding={0.5}
          >
            <Typography
              sx={(theme) => ({
                paddingX: theme.spacing(1),
              })}
              textAlign="left"
              variant="body1"
            >
              Educational Background
            </Typography>

            <Divider />
            {profileInfo.educationalInfo.map((edu) => (
              <EducationalInfoCard
                handleEdit={() => setSchool(edu)}
                key={edu.id}
                info={edu}
                handleRemove={() =>
                  setProfileInfo({
                    ...profileInfo,
                    educationalInfo: profileInfo.educationalInfo.filter(
                      (e) => e.id !== edu.id
                    ),
                  })
                }
              />
            ))}
          </Stack>
          <Stack
            spacing={1}
            component={Paper}
            elevation={2}
            width="100%"
            padding={0.5}
          >
            <Typography
              sx={(theme) => ({
                paddingX: theme.spacing(1),
              })}
              textAlign="left"
              variant="body1"
            >
              Work Experience
            </Typography>

            <Divider />

            {profileInfo.workExperience.map((wrk) => (
              <WorkExperienceInfoCard
                handleRemove={() =>
                  setProfileInfo({
                    ...profileInfo,
                    workExperience: profileInfo.workExperience.filter(
                      (w) => w.id !== wrk.id
                    ),
                  })
                }
                info={wrk}
                handleEdit={() => setWork(wrk)}
              />
            ))}
          </Stack>
        </Stack>
      </Stack>
    </Stack>
  );
}
