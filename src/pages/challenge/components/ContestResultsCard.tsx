import { Stack, Typography } from "@mui/material";
import React from "react";

interface IProps {
  text: string;
}
export default function ContestResultsCard({ text }: IProps) {
  return (
    <Stack
      direction="row"
      alignItems="center"
      justifyContent="center"
      padding={1}
    >
      <Typography textAlign="center" variant="h5">
        {text}
      </Typography>
    </Stack>
  );
}
