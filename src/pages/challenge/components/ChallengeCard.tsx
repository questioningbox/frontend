import { Button, Stack, Typography } from "@mui/material";
import dayjs from "dayjs";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useTimer } from "use-timer";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import controller from "../../../controller";
import { setCompetitions } from "../../../features/slice/CompetitionReducer";
import {
  responseFailed,
  responsePending,
  responseSuccessful,
} from "../../../features/slice/ResponseSlice";
import ApiResponseModel from "../../../models/ApiResponseModel";
import CompetitionModel from "../../../models/CompetitionModel";
import ApiRoutes from "../../../routes/ApiRoutes";
import NavigationRoutes from "../../../routes/NavigationRoutes";
import { CustomIconButton } from "../../../shared";
import { IFakeRequestIn } from "../services";

interface IProps {
  info: CompetitionModel;
}
export default function ChallengeCard({ info }: IProps) {
  const [eventTime, setEventTime] = useState<number>(0);
  const navigation = useNavigate();
  const { user } = useAppSelector((state) => state.UserReducer);
  const { competitions } = useAppSelector((state) => state.CompetitionsReducer);
  const dispatch = useAppDispatch();
  const { time, start, status, advanceTime, reset, pause } = useTimer({
    initialTime: 0,
    step: 1,
    onTimeUpdate: () => {
      setEventTime(dayjs(info.startDate).diff(dayjs().format(), "date"));
    },
  });

  useEffect(() => {
    start();
  }, []);

  async function handleJoinCompetition() {
    try {
      dispatch(responsePending());
      const res = await controller<ApiResponseModel<CompetitionModel>>({
        method: "put",
        url: ApiRoutes.competition.crud(`join/${info.competitionId}`),
        token: user?.token,
      });
      dispatch(responseSuccessful(res.message));
      dispatch(
        setCompetitions({
          ...competitions,
          results: competitions.results.map((c) => {
            if (c.competitionId === res.data.competitionId) {
              return res.data;
            } else {
              return c;
            }
          }),
        })
      );
    } catch (error) {
      dispatch(responseFailed(error));
    }
  }

  return (
    <Stack
      padding={1}
      direction="row"
      alignItems="center"
      justifyContent="space-between"
      spacing={1}
      border={(theme) => `1px solid ${theme.palette.action.disabledBackground}`}
      borderRadius={(theme) => theme.spacing(0.5)}
      sx={(theme) => ({
        [theme.breakpoints.down("sm")]: {
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "flex-start",
        },
      })}
    >
      <Stack
        sx={(theme) => ({
          [theme.breakpoints.down("sm")]: {
            alignItems: "flex-start",
            justifyContent: "center",
            width: "100%",
          },
        })}
        flex={1}
      >
        <Typography variant="body1">{info.title}</Typography>
        <Stack
          direction="row"
          spacing={1}
          alignItems="center"
          justifyContent="flex-start"
        >
          <Typography variant="caption">coming up on: </Typography>
          <Typography color="seagreen" variant="body2" component="p">
            {`${dayjs(info.startDate).format("DD MMMM YYYY")} - ${dayjs(
              info.startTime
            ).format("hh:mm a")}`}
          </Typography>
        </Stack>
      </Stack>
      <Stack
        spacing={1}
        alignItems="center"
        justifyContent="flex-end"
        direction="row"
        sx={(theme) => ({
          [theme.breakpoints.down("sm")]: {
            width: "100%",
          },
        })}
      >
        {user &&
          !info.competitors.includes(user?.userId) &&
          !info.request?.includes(user?.userId) &&
          info.createdBy !== user?.userId && (
            <CustomIconButton
              handleClick={handleJoinCompetition}
              size="small"
              variant="outlined"
              title="Join"
            />
          )}
        <CustomIconButton
          size="small"
          handleClick={() =>
            navigation(
              `${NavigationRoutes.challenge.details}?q=info&id=${info.competitionId}`
            )
          }
          variant="contained"
          title="Read More"
        />
      </Stack>
    </Stack>
  );
}
