import { Stack, Typography } from "@mui/material";
import React from "react";
import { IFakeProfile } from "../views/LeaderBoardPage";

interface IProps {
  info: IFakeProfile;
}
export default function ContestantProfleCard({ info }: IProps) {
  return (
    <Stack justifyContent="center" alignItems="center">
      <Stack width="50px" height="50px" overflow="hidden" borderRadius="50px">
        <img src={info.profileURL} alt="contestant-profile-avatar" />
      </Stack>
      <Typography textAlign="center" variant="body1">
        {info.name}
      </Typography>
      <Typography textAlign="center" variant="caption">
        {info.school}
      </Typography>
    </Stack>
  );
}
