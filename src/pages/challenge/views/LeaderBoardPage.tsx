import { Grid, Stack, Typography } from "@mui/material";
import React from "react";
import { AiOutlineClockCircle } from "react-icons/ai";
import { RiGroupLine } from "react-icons/ri";
import resources from "../../../resources";
import { TfiBook } from "react-icons/tfi";
import { getFakeStudent, IFakeStudent } from "../../student/services";
import {
  ChallengeDetailCard,
  ChallengeHeaderTag,
  ContestantProfileCard,
  ContestResultsCard,
} from "../components";
import { AppColors } from "../../../constants";
import { CiBookmarkCheck } from "react-icons/ci";

export interface IFakeProfile extends IFakeStudent {
  school: string;
}

export default function LeaderBoardPage() {
  return (
    <Stack width="100%" height="100%">
      <Stack
        sx={(theme) => ({
          backgroundImage: `linear-gradient(rgba(0,0,0,0.45),rgba(0,0,0,0.25)),url(${resources.trophy})`,
          backgroundSize: "contain",
          minHeight: "200px",
          backgroundRepeat: "no-repeat",
          backgroundPosition: "center",
          [theme.breakpoints.down("sm")]: {
            backgroundImage: `linear-gradient(rgba(0,0,0,0.45),rgba(0,0,0,0.25))`,
            minHeight: "200px",
          },
        })}
        paddingX={(theme) => theme.spacing(4)}
        alignItems="baseline"
        width="100%"
        justifyContent="space-between"
      >
        <ChallengeHeaderTag />
        <Stack
          bgcolor={(theme) => theme.palette.common.white}
          marginBottom={(theme) => theme.spacing(-6)}
          padding={(theme) => theme.spacing(1.5)}
          alignSelf="center"
          width="100%"
        >
          <Stack
            spacing={2.5}
            direction="row"
            alignItems="center"
            justifyContent="center"
            sx={(theme) => ({
              [theme.breakpoints.down("sm")]: {
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "center",
              },
            })}
          >
            <ContestantProfileCard
              info={{
                school: "University of Energy and Natural Resources",
                ...getFakeStudent(),
              }}
            />
            <ContestResultsCard text="84% - 76%" />
            <ContestantProfileCard
              info={{
                ...getFakeStudent(),
                school: "Kwame Nkrumah University of Science and Technology",
              }}
            />
          </Stack>
        </Stack>
      </Stack>
      <Stack marginTop={6} padding={1.5} paddingX={4} width="100%">
        <Typography textAlign="center" fontWeight="bold" variant="body1">
          Challenge Details
        </Typography>
        <Stack
          border={(theme) => `1px solid ${theme.palette.action.hover}`}
          borderRadius={(theme) => theme.spacing(0.5)}
          padding={(theme) => theme.spacing(1)}
          marginY={(theme) => theme.spacing(2)}
        >
          <Grid
            container
            alignItems="center"
            width="100%"
            justifyContent="center"
            spacing={1}
            rowSpacing={2}
            columnSpacing={2}
          >
            <ChallengeDetailCard
              Icon={<RiGroupLine style={{ color: "lightgreen" }} />}
              title="Challenge Type"
              subtitle="one on one"
            />
            <ChallengeDetailCard
              Icon={<AiOutlineClockCircle style={{ color: "orange" }} />}
              title="Duration"
              subtitle={Math.floor(Math.random() * 10) + "hrs"}
            />
            <ChallengeDetailCard
              Icon={<TfiBook style={{ color: AppColors.primary }} />}
              title="Subject Area"
              subtitle="Quantum Physics"
            />
            <ChallengeDetailCard
              Icon={<CiBookmarkCheck style={{ color: "firebrick" }} />}
              title="Author"
              subtitle="Prof. Mensah Ransford"
            />
          </Grid>
        </Stack>
      </Stack>
    </Stack>
  );
}
