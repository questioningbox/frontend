import { CreateGroupDto } from "./../../../dto/CreateGroupDto";

export const tagRegex = /^[a-zA-Z0-9]/;

export function validateCreateGroupInfo(info: CreateGroupDto) {
  if (info.title.length <= 0) {
    throw "Group Title Required";
  }

  if (!tagRegex.test(info.tag)) {
    throw "invalid tag, tag can only contain, numbers and alphabets";
  }
}
