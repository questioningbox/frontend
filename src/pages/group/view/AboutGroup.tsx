import { colors, Stack, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import { AiOutlineUserAdd } from "react-icons/ai";
import {
  useLocation,
  useNavigate,
  useParams,
  useSearchParams,
} from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import controller from "../../../controller";
import {
  responseFailed,
  responsePending,
  responseSuccessful,
} from "../../../features/slice/ResponseSlice";
import ApiResponseModel from "../../../models/ApiResponseModel";
import { GroupInfoModel } from "../../../models/GroupModel";
import resources from "../../../resources";
import ApiRoutes from "../../../routes/ApiRoutes";
import { CustomIconButton } from "../../../shared";

export default function AboutGroup() {
  const { groupdId } = useParams();
  const location = useLocation();
  const [searchParams, setSearchParams] = useSearchParams();
  const navigation = useNavigate();
  const [info, setInfo] = useState<GroupInfoModel | null>(null);
  const dispatch = useAppDispatch();
  useEffect(() => {
    if (!searchParams.get("groupId")) {
      return navigation("/groups");
    }
    getGroupInfo();
  }, []);

  //

  async function getGroupInfo() {
    try {
      dispatch(responsePending());
      const res = await controller<ApiResponseModel<GroupInfoModel>>({
        method: "get",
        url: ApiRoutes.group.crud(searchParams.get("groupId")),
      });
      dispatch(responseSuccessful(res.message));
      setInfo(res.data);
    } catch (error) {
      dispatch(responseFailed(error));
    }
  }

  useEffect(() => {
    getGroupInfo();
  }, [searchParams.get("groupId")]);

  //
  return (
    <Stack width="100%">
      <Stack
        height="30vh"
        sx={(theme) => ({
          backgroundImage: `linear-gradient(rgba(0,0,0,0.65),rgba(0,0,0,0.75),rgba(0,0,0,0.85)),url(${resources.groupinfobanner})`,
          backgroundPosition: "center",
          backgroundSize: "cover",
          backgroundRepeat: "no-repeat",
        })}
        alignItems="center"
        justifyContent="center"
      >
        <Typography
          sx={(theme) => ({
            color: theme.palette.common.white,
            fontSize: theme.spacing(3),
            textAlign: "center",
          })}
          variant="body1"
        >
          {info?.title}
        </Typography>
        <Typography
          sx={(theme) => ({
            fontSize: theme.spacing(2),
            color: colors.grey[100],
          })}
          variant="body2"
        >
          createdBy: {info?.creator?.name}
        </Typography>
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="center"
          width="100%"
          spacing={1}
          marginBottom={1.5}
        >
          <Typography
            variant="body1"
            color={(theme) => theme.palette.common.white}
          >
            Members:
          </Typography>
          <Typography
            variant="body1"
            color={(theme) => theme.palette.common.white}
          >
            {info?.members.length}
          </Typography>
        </Stack>
        <CustomIconButton
          size="small"
          variant="outlined"
          title="Join"
          Icon={AiOutlineUserAdd}
          props={{
            style: { color: colors.grey[50], borderColor: colors.grey[50] },
          }}
        />
      </Stack>
    </Stack>
  );
}
