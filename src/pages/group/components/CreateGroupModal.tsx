import { Dialog, DialogContent, DialogTitle, Stack } from "@mui/material";
import React, { ChangeEvent, useState } from "react";
import { AiOutlineUserAdd } from "react-icons/ai";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import {
  CustomImagePicker,
  CustomPrimaryButton,
  InputGroup,
  RowContainer,
  SmallInput,
} from "../../../components";
import controller from "../../../controller";
import { CreateGroupDto } from "../../../dto/CreateGroupDto";
import {
  responseFailed,
  responsePending,
  responseSuccessful,
} from "../../../features/slice/ResponseSlice";
import { GroupThunk } from "../../../functions/slice";
import { IDialogBaseProps } from "../../../interface";
import UserModel from "../../../models/UserModel";
import ApiRoutes from "../../../routes/ApiRoutes";
import {
  BubbleInfoCard,
  CustomIconButton,
  DialogHeader,
} from "../../../shared";
import { validateCreateGroupInfo } from "../services";

interface IProps extends IDialogBaseProps {}
export default function CreateGroupModal({ open, handleClose }: IProps) {
  const { loading } = useAppSelector((state) => state.ResponseReducer);
  const dispatch = useAppDispatch();
  const [info, setInfo] = useState<CreateGroupDto>({
    title: "",
    tag: "",
    description: "",
    invites: [],
    createdBy: "",
    admins: [],
    members: [],
  });
  const { user } = useAppSelector((state) => state.UserReducer);
  const [username, setUsername] = useState<string>("");
  const [coverImage, setCoverImage] = useState<File | null>(null);
  async function AddInvite() {
    try {
      dispatch(responsePending());
      const res = await controller<UserModel>({
        method: "get",
        url: ApiRoutes.user.getUserByUsername(username),
        token: user?.token,
      });
      dispatch(responseSuccessful(""));
      setInfo({ ...info, invites: [...info.invites, res] });
    } catch (error) {
      console.log(error);
      dispatch(responseFailed(error));
    }
  }

  async function CreateGroup() {
    try {
      validateCreateGroupInfo(info);
      const inf: CreateGroupDto = {
        ...info,
        members: user ? [user?.userId] : [],
        createdBy: user?.userId ?? "",
        admins: user ? [user?.userId] : [],
      };
      const formData = new FormData();
      formData.append("file", coverImage as any);
      formData.append("tag", inf.tag);
      formData.append("title", inf.title);
      formData.append("description", inf.description);
      formData.append("createdBy", inf.createdBy);
      formData.append("invites", JSON.stringify(inf.invites));
      formData.append("admins", JSON.stringify(inf.admins));
      formData.append("members", JSON.stringify(inf.members));
      dispatch(
        GroupThunk({
          data: formData,
          isFile: true,
          url: ApiRoutes.group.crud(),
          method: "post",
          token: user?.token,
        })
      );
    } catch (error) {
      dispatch(responseFailed(error));
    }
  }

  return (
    <Dialog open={open} fullWidth maxWidth="sm">
      <DialogTitle>
        <DialogHeader title="Create Group" handleClose={handleClose} />
      </DialogTitle>
      <DialogContent dividers>
        <Stack width="100%" spacing={1} paddingBottom={4}>
          <InputGroup
            handleChange={(e) => setInfo({ ...info, title: e.target.value })}
            placeholder="tilte"
            label="Title*"
            props={{ value: info.title }}
          />
          <InputGroup
            handleChange={(e) => setInfo({ ...info, tag: e.target.value })}
            placeholder="tag"
            label="Tag"
            props={{ value: info.tag }}
          />
          <InputGroup
            props={{ multiline: true, value: info.description }}
            placeholder="group description"
            label="Description"
            handleChange={(e) =>
              setInfo({ ...info, description: e.target.value })
            }
          />
          <Stack />
          <RowContainer>
            <>
              <SmallInput
                style={{ flex: 1 }}
                label="Friend"
                placeholder="phone/email"
                handleChange={(e) => setUsername(e.target.value)}
              />
              <CustomIconButton
                Icon={AiOutlineUserAdd}
                title="Add"
                size="small"
                variant="outlined"
                props={{ disabled: !Boolean(username) || loading }}
                handleClick={AddInvite}
              />
            </>
          </RowContainer>
          <Stack>
            {info.invites.map((invite) => (
              <BubbleInfoCard
                title={invite.name}
                handleRemove={() =>
                  setInfo({
                    ...info,
                    invites: info.invites.filter(
                      (i) => i.userId !== invite.userId
                    ),
                  })
                }
              />
            ))}
          </Stack>
          <CustomImagePicker
            removeImage={() => {}}
            label="Choose Group Profile Picture"
            handleChange={(e: ChangeEvent<HTMLInputElement>) => {
              if (e.target.files && e.target.files.length > 0) {
                setCoverImage(e.target.files[0]);
              } else {
                setCoverImage(null);
              }
            }}
          />
          <CustomPrimaryButton
            variant="contained"
            title={loading ? "loading.." : "Create"}
            fullWidth
            handleClick={CreateGroup}
            props={{ disabled: loading }}
          />
        </Stack>
      </DialogContent>
    </Dialog>
  );
}
