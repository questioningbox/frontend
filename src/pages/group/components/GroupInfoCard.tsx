import {
  colors,
  IconButton,
  MenuItem,
  Paper,
  Stack,
  Typography,
} from "@mui/material";
import React from "react";
import { AiOutlineUserAdd } from "react-icons/ai";
import { BiMessageAltDetail } from "react-icons/bi";
import { BsBookmarkHeart } from "react-icons/bs";
import { CiBookmarkCheck, CiBookmarkPlus } from "react-icons/ci";
import { IoMdCheckmark } from "react-icons/io";
import { IoPersonRemoveOutline } from "react-icons/io5";
import { MdGroup } from "react-icons/md";
import { TbUserCheck } from "react-icons/tb";
import { useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import controller from "../../../controller";
import { setGroups } from "../../../features/slice/GroupsReducer";
import {
  responseFailed,
  responsePending,
  responseSuccessful,
} from "../../../features/slice/ResponseSlice";
import { PagedResults } from "../../../models";
import ApiResponseModel from "../../../models/ApiResponseModel";
import GroupModel from "../../../models/GroupModel";
import ApiRoutes from "../../../routes/ApiRoutes";
import { CustomIconButton } from "../../../shared";

interface IProps {
  info: GroupModel;
}
export default function GroupInfoCard({ info }: IProps) {
  const { user } = useAppSelector((state) => state.UserReducer);
  const dispatch = useAppDispatch();
  const { groups } = useAppSelector((state) => state.GroupsReducer);
  const navigation = useNavigate();
  //
  async function handleInvite(status: string) {
    try {
      dispatch(responsePending());
      const res = await controller<ApiResponseModel<GroupModel>>({
        method: "put",
        url: ApiRoutes.group.crud(`invite/${info.groupId}/${status}`),
        token: user?.token,
      });
      dispatch(responseSuccessful(res.message));
      const data: PagedResults<GroupModel> = {
        ...groups,
        results: groups.results.map((g) =>
          g.groupId === info.groupId ? res.data : g
        ),
      };
      dispatch(setGroups(data));
    } catch (error) {
      dispatch(responseFailed(error));
    }
  }
  return (
    <Paper style={{ width: "100%" }} elevation={0} variant="outlined">
      <MenuItem
        onClick={() => navigation(`/groups/info?groupId=${info.groupId}`)}
        style={{ width: "100%", padding: 0, overflow: "wrap" }}
      >
        <Stack
          padding={1}
          width="100%"
          borderRadius={(theme) => theme.spacing(0.5)}
        >
          <Stack width="100%">
            <Typography
              textOverflow="text-wrap"
              textAlign="left"
              variant="body1"
            >
              {info.title}
            </Typography>
          </Stack>
          <Stack
            direction="row"
            alignItems="center"
            justifyContent="space-between"
            spacing={1}
          >
            <Stack
              direction="row"
              spacing={1}
              alignItems="center"
              justifyContent="flex-start"
            >
              <Typography variant="caption">#{info.tag}</Typography>
              <Stack direction="row" spacing={0.5}>
                <MdGroup />
                <Typography variant="body2">{info.members.length}</Typography>
              </Stack>
            </Stack>

            <Stack spacing={1} direction="row">
              {user && info.invites.includes(user?.userId) && (
                <CustomIconButton
                  size="xsmall"
                  variant="outlined"
                  title="Decline"
                  Icon={IoPersonRemoveOutline}
                  handleClick={() => handleInvite("0")}
                  props={{
                    style: { color: "firebrick", borderColor: "firebrick" },
                  }}
                />
              )}
              {user &&
                !info.invites.includes(user?.userId.toString()) &&
                info.createdBy !== user?.userId && (
                  <CustomIconButton
                    size="xsmall"
                    variant="outlined"
                    title="Join"
                    Icon={AiOutlineUserAdd}
                  />
                )}

              {user && info.invites.includes(user?.userId.toString()) && (
                <CustomIconButton
                  size="xsmall"
                  variant="outlined"
                  title="Accept"
                  Icon={TbUserCheck}
                  handleClick={() => handleInvite("1")}
                />
              )}
            </Stack>
          </Stack>
        </Stack>
      </MenuItem>
    </Paper>
  );
}
