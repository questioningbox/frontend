import { Stack } from "@mui/material";
import React from "react";
import { Outlet } from "react-router-dom";

export default function RootPage() {
  return (
    <Stack flex={1}>
      <Outlet />
    </Stack>
  );
}
