import {
  Button,
  Divider,
  LinearProgress,
  Stack,
  Typography,
} from "@mui/material";
import dayjs from "dayjs";
import React, { useEffect, useState } from "react";
import { FiChevronRight } from "react-icons/fi";
import { IoTimeOutline } from "react-icons/io5";
import { RiHome6Line } from "react-icons/ri";
import { useNavigate } from "react-router-dom";
import { useTimer } from "use-timer";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { AlertModal, ExamsQuestionCard, TextLink } from "../../../components";
import { clearResponseMessages } from "../../../features/slice/ContestReducer";
import {
  ContestThunk,
  GetContestQuestionThunk,
} from "../../../functions/slice";
import { ContestAnswer } from "../../../models/ContestModel";
import ApiRoutes from "../../../routes/ApiRoutes";
import NavigationRoutes from "../../../routes/NavigationRoutes";
import { CustomIconButton } from "../../../shared";

export default function TestPage() {
  const dispatch = useAppDispatch();
  const [timeOver, setIsTimeOver] = useState(false);
  const { time, start, pause, reset, status } = useTimer({
    initialTime: 60,
    timerType: "DECREMENTAL",
    step: 1,
    endTime: 0,
    interval: 1000,
    onTimeOver: () => setIsTimeOver(true),
    onTimeUpdate: (t) => {
      if (t !== 0) {
        setIsTimeOver(false);
      }
    },
  });

  const [answer, setAnswer] = useState<ContestAnswer>({
    answer: [],
    questionId: "",
    mark: 0,
    score: 0,
    selection: [],
  });
  const { errorMessage, message, activeQuestion, contest } = useAppSelector(
    (state) => state.ContestReducer
  );
  const navigation = useNavigate();
  const { user } = useAppSelector((state) => state.UserReducer);

  function handleTimeOver() {}

  useEffect(() => {
    if (activeQuestion) {
      reset();
      start();
    }
  }, [activeQuestion]);

  function handleNextQuestion() {
    pause();
    dispatch(
      GetContestQuestionThunk({
        method: "put",
        url: ApiRoutes.contest.crud(`answer/${contest?.contestId}`),
        token: user?.token,
        data: { ...answer, questionId: activeQuestion?.question.questionId },
      })
    );
  }

  // useEffect(() => {
  //   if (!contest) {
  //     return navigation(NavigationRoutes.preptest.guide);
  //   }
  //   if (contest.status !== "active") {
  //     navigation(NavigationRoutes.preptest.results);
  //   }
  // }, [contest]);

  useEffect(() => {}, [activeQuestion]);

  function gradeContest() {
    dispatch(
      ContestThunk({
        method: "get",
        url: ApiRoutes.contest.crud(`grade/${contest?.contestId}`),
        token: user?.token,
      })
    );
    navigation(NavigationRoutes.preptest.results);
  }

  function resetTrialTest() {
    dispatch(
      GetContestQuestionThunk({
        method: "get",
        url: ApiRoutes.contest.crud(`reset/${contest?.contestId}`),
        token: user?.token,
      })
    );
  }

  return (
    <Stack
      paddingX={2}
      height="100%"
      width="100%"
      sx={(theme) => ({
        overflowY: "auto",
      })}
      spacing={1}
    >
      <AlertModal
        isError={errorMessage ? errorMessage : undefined}
        isMessage={message ? message : undefined}
        text={errorMessage || message}
        action={
          <Stack
            direction="row"
            alignItems="center"
            justifyContent="center"
            spacing={1}
          >
            <CustomIconButton
              title="Close"
              variant="outlined"
              size="small"
              handleClick={() => dispatch(clearResponseMessages())}
            />
          </Stack>
        }
      />
      <Stack
        paddingY={1.5}
        direction="row"
        alignItems="center"
        justifyContent="space-between"
      >
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="flex-start"
          spacing={1}
        >
          <RiHome6Line size={15} style={{ color: "#c0c0c0" }} />
          <FiChevronRight style={{ color: "#c0c0c0" }} />
          <Typography variant="body2">Trial tests</Typography>
          <FiChevronRight style={{ color: "#c0c0c0" }} />
          <Typography variant="body2">...</Typography>
          <FiChevronRight style={{ color: "#c0c0c0" }} />
          <TextLink
            text={user?.name || ""}
            route={NavigationRoutes.preptest.results}
          />
        </Stack>
        {activeQuestion && (
          <Stack
            direction="row"
            alignItems="center"
            justifyContent="center"
            spacing={0.45}
            sx={(theme) => ({
              padding: theme.spacing(0.45),
              borderRadius: theme.spacing(0.35),
              border: `1px solid ${theme.palette.action.hover}`,
            })}
          >
            <IoTimeOutline
              size={13}
              color={time > 25 ? "primary" : time > 15 ? "orange" : "red"}
            />
            <Typography
              color={time > 25 ? "primary" : time > 15 ? "orange" : "red"}
              variant="caption"
            >
              {`${time} seconds`}
            </Typography>
          </Stack>
        )}
      </Stack>
      {activeQuestion && (
        <Stack padding={(theme) => theme.spacing(0.5, 0)}>
          <LinearProgress
            value={contest?.solved.length || 0}
            variant="determinate"
            color="primary"
          />
        </Stack>
      )}
      {activeQuestion && (
        <ExamsQuestionCard
          isTimeOver={timeOver}
          handleAnswer={(ans) =>
            setAnswer({
              questionId: activeQuestion.question.questionId,
              score: 0,
              mark: 0,
              answer: ans,
              selection: [],
            })
          }
          info={activeQuestion}
        />
      )}
      {!activeQuestion && contest && contest.questions.length <= 0 && (
        <Stack spacing={1} padding={(theme) => theme.spacing(2, 0)}>
          <CustomIconButton
            title="Start Again"
            variant="outlined"
            size="small"
            handleClick={resetTrialTest}
          />
          <CustomIconButton
            size="small"
            variant="outlined"
            title="View Results"
            handleClick={gradeContest}
          />
        </Stack>
      )}
      {activeQuestion && (
        <Stack height="100%" width="100%">
          <Stack
            direction="row"
            alignItems="center"
            justifyContent="space-between"
            padding={(theme) => theme.spacing(1, 0)}
            width="100%"
            borderTop={(theme) =>
              `1px solid ${theme.palette.action.disabledBackground}`
            }
          >
            <Button
              sx={(theme) => ({
                textTransform: "none",
              })}
              variant="outlined"
              size="small"
              color="primary"
              disabled
            >
              Previous
            </Button>
            <Stack direction="row" alignItems="center" justifyContent="center">
              {contest && (
                <Typography variant="body2">
                  Page {contest?.solved.length} of{" "}
                  {contest?.solved.length + contest?.questions.length}
                </Typography>
              )}
            </Stack>
            <Button
              sx={(theme) => ({
                textTransform: "none",
              })}
              variant="outlined"
              size="small"
              color="primary"
              onClick={handleNextQuestion}
            >
              Next
            </Button>
          </Stack>
        </Stack>
      )}
    </Stack>
  );
}
