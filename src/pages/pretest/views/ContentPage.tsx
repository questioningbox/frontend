import {
  Button,
  Grid,
  IconButton,
  MenuItem,
  Stack,
  TextField,
  Typography,
  useMediaQuery,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { BsListUl } from "react-icons/bs";
import { useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { PrimaryLinkButton } from "../../../components";
import { CourseThunk } from "../../../functions/slice";
import ApiRoutes from "../../../routes/ApiRoutes";
import NavigationRoutes from "../../../routes/NavigationRoutes";
import { PreptestMenu } from "../../../shared/components";
import { getStoreItems } from "../../../utilities";
import { AppFooter, CourseInfoCard, StoreItem } from "../../../views";

export default function ContentPage() {
  const isMobile = useMediaQuery("(max-width:567px)");
  const [menu, setMenu] = useState<HTMLElement | null>(null);
  const navigation = useNavigate();
  const dispatch = useAppDispatch();
  const { courses } = useAppSelector((state) => state.CoursesReducer);

  useEffect(() => {
    dispatch(CourseThunk({ method: "get", url: ApiRoutes.course.crud }));
  }, []);
  return (
    <Stack height="100%">
      <PreptestMenu anchorEl={menu} handleClose={() => setMenu(null)} />
      <Stack
        width="100%"
        padding={2}
        alignItems="center"
        justifyContent="center"
      >
        <Typography textAlign="center" variant="h5">
          Trial Prep Test: Lets Do This
        </Typography>
        <Typography textAlign="center" variant="caption">
          Level up your skills by taking this test and become ready for your
          upcoming exams
        </Typography>
        <PrimaryLinkButton
          text="Get started"
          route={NavigationRoutes.preptest.guide}
        />
      </Stack>
      <Stack
        width="100%"
        direction="row"
        alignItems="center"
        justifyContent="space-between"
        bgcolor={(theme) => theme.palette.action.hover}
        padding={1.5}
      >
        {!isMobile && (
          <Stack
            direction="row"
            alignItems="center"
            justifyContent="flex-start"
          >
            {[
              "View All",
              "Design",
              "Product",
              "Software Engineering",
              "Customer Services",
            ].map((link) => (
              <Button
                variant="text"
                sx={(theme) => ({
                  textTransform: "none",
                })}
                size="small"
                color="inherit"
              >
                {link}
              </Button>
            ))}
          </Stack>
        )}
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="center"
          spacing={1}
          sx={(theme) => ({
            [theme.breakpoints.down("sm")]: {
              width: "100%",
            },
          })}
        >
          <TextField
            sx={(theme) => ({
              minWidth: "200px",
            })}
            select
            variant="outlined"
            size="small"
            defaultValue="Most Recent"
            InputProps={{
              className: "small-input",
            }}
          >
            <MenuItem value="Most Recent">Most recent</MenuItem>
          </TextField>
          {isMobile && (
            <IconButton onClick={(e) => setMenu(e.currentTarget)} size="small">
              <BsListUl />
            </IconButton>
          )}
        </Stack>
      </Stack>
      <Stack padding={1} flex={1}>
        <Grid
          alignItems="flex-start"
          justifyContent="center"
          container
          spacing={1}
        >
          {courses.results.map((item) => (
            <CourseInfoCard
              parentProps={{ borderRadius: (theme) => theme.spacing(0.5) }}
              info={item}
            />
          ))}
        </Grid>
      </Stack>
    </Stack>
  );
}
