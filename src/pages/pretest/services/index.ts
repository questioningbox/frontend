import { ContestAnswer } from "../../../models/ContestModel";

export function calculateTotalScore(answers: ContestAnswer[]): {
  totalMark: number;
  score: number;
  average: number;
} {
  let score = 0;
  let totalScores = 0;
  answers.map((ans) => {
    score += ans.score;
    totalScores += ans.mark;
  });
  return {
    totalMark: totalScores,
    score,
    average: score / (totalScores === 0 ? 1 : totalScores),
  };
}
