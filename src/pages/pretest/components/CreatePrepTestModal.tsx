import {
  Dialog,
  DialogContent,
  DialogTitle,
  MenuItem,
  Stack,
} from "@mui/material";
import React from "react";
import { useAppSelector } from "../../../app/hooks";
import { CustomPrimaryButton, InputGroup } from "../../../components";
import { IDialogBaseProps } from "../../../interface";
import { DialogHeader } from "../../../shared";

interface IProps extends IDialogBaseProps {
  handleInit: () => void;
  handleCategory: (cate: string) => void;
}
export default function CreatePrepTestModal({
  open,
  handleClose,
  handleInit,
  handleCategory,
}: IProps) {
  const { features } = useAppSelector((state) => state.FeaturesReducer);
  return (
    <Dialog open={open}>
      <DialogTitle>
        <DialogHeader title="Create Trial Test" handleClose={handleClose} />
      </DialogTitle>
      <DialogContent dividers>
        <Stack spacing={1}>
          <InputGroup
            handleChange={(e) => handleCategory(e.target.value)}
            label="Subject"
            props={{ select: true }}
          >
            {features.questionsCategories.map((ca) => (
              <MenuItem value={ca.categoryId} key={ca._id}>
                {ca.title}
              </MenuItem>
            ))}
          </InputGroup>
          <CustomPrimaryButton
            variant="contained"
            title="Create Test"
            handleClick={handleInit}
          />
        </Stack>
      </DialogContent>
    </Dialog>
  );
}
