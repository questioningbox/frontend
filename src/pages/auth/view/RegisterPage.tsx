import { Visibility, VisibilityOffOutlined } from "@mui/icons-material";
import {
  Stack,
  Typography,
  TextField,
  InputAdornment,
  IconButton,
  Button,
  MenuItem,
  Chip,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { BiEnvelope } from "react-icons/bi";
import { FiLock, FiPhone } from "react-icons/fi";
import { BsArrowLeftShort } from "react-icons/bs";
import NavigationRoutes from "../../../routes/NavigationRoutes";
import { useNavigate } from "react-router-dom";
import { AiOutlineUser } from "react-icons/ai";
import { RegisterViaEmailDto } from "../../../dto/Auth";
import { UserAccountTypes } from "../data";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { ResponseLabel } from "../../../components";
import { AuthThunk } from "../../../functions/auth";
import ApiRoutes from "../../../routes/ApiRoutes";
import {
  IValidationError,
  ValidateRegisterViaEmailDto,
} from "../../../features/auth/Validation";
import {
  CustomCountryPicker,
  CustomOutlinedCountryPicker,
} from "../../../shared";
///
export default function RegisterPage() {
  const { features } = useAppSelector((state) => state.FeaturesReducer);
  const [showPassword, setShowPassword] = useState<Boolean>(false);
  const [validationError, setValidationError] = useState<IValidationError>({
    name: "",
    error: null,
  });
  const dispatch = useAppDispatch();
  const { loading } = useAppSelector((state) => state.ResponseReducer);
  const [registerInfo, setRegisterInfo] = useState<RegisterViaEmailDto>({
    userType: "student",
    email: "",
    password: "",
    name: "",
    country: null,
  });
  const navigation = useNavigate();
  function handlePasswordVisibility() {
    setShowPassword(!showPassword);
  }

  function handleRegisterViaEmail() {
    try {
      ValidateRegisterViaEmailDto(registerInfo);
      dispatch(
        AuthThunk({
          data: registerInfo,
          method: "post",
          url: ApiRoutes.auth.register.email,
        })
      );
    } catch (error) {
      setValidationError(error as IValidationError);
    }
  }

  return (
    <Stack
      height="100%"
      width="100%"
      alignItems="center"
      justifyContent="center"
      sx={(theme) => ({
        [theme.breakpoints.down("sm")]: {
          padding: theme.spacing(2),
        },
        padding: theme.spacing(4),
      })}
      spacing={2}
    >
      <Typography fontWeight={900} variant="h5">
        Create Account
      </Typography>
      <Stack
        spacing={1.75}
        sx={(theme) => ({
          [theme.breakpoints.down("sm")]: {
            width: "100%",
          },
          width: "80%",
        })}
      >
        <Stack width="100%" spacing={0.5}>
          <Typography variant="body1">Name*</Typography>
          <TextField
            size="small"
            autoComplete="off"
            fullWidth
            variant="outlined"
            color="primary"
            placeholder="Full Name"
            type="text"
            error={validationError.name === "name"}
            name="name"
            onChange={(e) =>
              setRegisterInfo({ ...registerInfo, name: e.target.value })
            }
            required
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <AiOutlineUser />
                </InputAdornment>
              ),
            }}
          />
          {validationError.name === "name" && (
            <Typography variant="caption" color="error" textAlign="center">
              {validationError.error}
            </Typography>
          )}
        </Stack>
        <Stack spacing={0.5}>
          <Typography variant="body1">Email*</Typography>
          <TextField
            size="small"
            autoComplete="off"
            fullWidth
            variant="outlined"
            color="primary"
            placeholder="Email Address"
            type="email"
            onChange={(e) =>
              setRegisterInfo({ ...registerInfo, email: e.target.value })
            }
            name="email"
            required
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <BiEnvelope />
                </InputAdornment>
              ),
            }}
            error={validationError.name === "email"}
          />
          {validationError.name === "email" && (
            <Typography variant="caption" color="error" textAlign="center">
              {validationError.error}
            </Typography>
          )}
        </Stack>

        <Stack spacing={0.5}>
          <Typography variant="body1">Password</Typography>
          <TextField
            size="small"
            fullWidth
            variant="outlined"
            color="primary"
            placeholder="Password"
            type={showPassword ? "text" : "password"}
            name="password"
            onChange={(e) =>
              setRegisterInfo({ ...registerInfo, password: e.target.value })
            }
            required
            autoComplete="off"
            error={validationError.name === "password"}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <FiLock />
                </InputAdornment>
              ),
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton onClick={handlePasswordVisibility}>
                    {showPassword ? <Visibility /> : <VisibilityOffOutlined />}
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
          {validationError.name === "password" && (
            <Typography variant="caption" color="error" textAlign="center">
              {validationError.error}
            </Typography>
          )}
        </Stack>
        <Stack spacing={1}>
          <Typography variant="body1">User Type</Typography>
          <TextField
            size="small"
            autoComplete="off"
            fullWidth
            variant="outlined"
            color="primary"
            label="User Type"
            select
            value={registerInfo.userType}
            onChange={(e) =>
              setRegisterInfo({
                ...registerInfo,
                userType: e.target.value as any,
              })
            }
          >
            <MenuItem value="">None</MenuItem>
            {features.usersCategory.map((cat) => (
              <MenuItem key={cat._id} value={cat.title}>
                {cat.title}
              </MenuItem>
            ))}
          </TextField>
        </Stack>
        <CustomOutlinedCountryPicker
          label="Country"
          handleChange={(e) => setRegisterInfo({ ...registerInfo, country: e })}
        />
        {/* <CustomCountryPicker handleChange={(e) => console.log("country ", e)} /> */}
        <Button
          sx={(theme) => ({
            textTransform: "none",
          })}
          disableElevation
          variant="contained"
          size="medium"
          color="primary"
          onClick={handleRegisterViaEmail}
          disabled={loading || !registerInfo.country}
        >
          {loading ? "Loading...." : "Sign Up"}
        </Button>

        <Button
          sx={(theme) => ({
            textTransform: "none",
          })}
          variant="outlined"
          size="medium"
          color="primary"
          onClick={() => navigation(NavigationRoutes.account.registerByPhone)}
        >
          Sign Up With PhoneNumber
        </Button>

        <Stack
          direction="row"
          marginY={3}
          alignItems="center"
          justifyContent="center"
          width="100%"
        >
          <Chip
            sx={(theme) => ({
              bgcolor: theme.palette.common.white,
              borderRadius: theme.spacing(0),
              borderWidth: 0,
            })}
            onClick={() => navigation(NavigationRoutes.account.login)}
            size="small"
            avatar={<BsArrowLeftShort />}
            label={
              <Typography color="primary" variant="body2">
                Back to login
              </Typography>
            }
          />
        </Stack>
        {/* <ResponseLabel /> */}
      </Stack>
    </Stack>
  );
}
