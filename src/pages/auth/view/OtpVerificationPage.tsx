/* eslint-disable react-hooks/exhaustive-deps */
import { Stack, Typography, Button } from "@mui/material";
import React, { useState } from "react";

import { useNavigate } from "react-router-dom";
import OtpInput from "react-otp-input";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { AuthThunk } from "../../../functions/auth";
import ApiRoutes from "../../../routes/ApiRoutes";
import { ResponseLabel } from "../../../components";

///
export default function OtpVerificationPage() {
  const { user } = useAppSelector((state) => state.UserReducer);
  const navigation = useNavigate();
  const [code, setCode] = useState<string>("******");
  const { loading } = useAppSelector((state) => state.ResponseReducer);
  const dispatch = useAppDispatch();

  async function resendOTP() {
    if (user) {
      dispatch(
        AuthThunk({
          data: {
            email: user.email,
          },
          method: "put",
          url: ApiRoutes.auth.resendOtp(user.userId),
        })
      );
    }
  }

  function handleOtpVerification() {
    if (user) {
      dispatch(
        AuthThunk({
          data: { code },
          method: "put",
          token: user?.token,
          url: ApiRoutes.auth.verifyOtp(user.userId),
        })
      );
    }
  }

  return (
    <Stack
      height="100%"
      width="100%"
      alignItems="center"
      justifyContent="center"
      sx={(theme) => ({
        [theme.breakpoints.down("sm")]: {
          padding: theme.spacing(2),
        },
        padding: theme.spacing(4),
      })}
      spacing={2}
    >
      <Typography fontWeight={900} variant="h4">
        Verify Account
      </Typography>

      <Stack
        alignItems="center"
        justifyContent="center"
        width="100%"
        direction="row"
      >
        <OtpInput
          numInputs={6}
          value={code}
          onChange={(val: any) => setCode(val)}
          separator={<Stack marginX={0.5}>-</Stack>}
          inputStyle={{
            width: "50px",
            height: "40px",
            border: "1px solid #d0d0d0",
            borderRadius: "5px",
            fontSize: "18px",
            fontWeight: "bold",
          }}
        />
      </Stack>

      <Typography textAlign="center" variant="caption">
        Enter the 6-digit code sent to your account
      </Typography>

      <Button
        sx={(theme) => ({
          textTransform: "none",
        })}
        disableElevation
        variant="contained"
        size="medium"
        color="primary"
        fullWidth
        onClick={handleOtpVerification}
        disabled={loading}
      >
        {loading ? "Loading...." : "Submit"}
      </Button>

      <Button
        sx={(theme) => ({
          textTransform: "none",
        })}
        disableElevation
        variant="text"
        size="medium"
        color="primary"
        fullWidth
        onClick={resendOTP}
      >
        Resend Code
      </Button>
      <ResponseLabel />
    </Stack>
  );
}
