import { Stack, Typography, InputAdornment, Chip } from "@mui/material";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import NavigationRoutes from "../../../routes/NavigationRoutes";
import { BsArrowLeftShort } from "react-icons/bs";
import { InputGroup, PrimaryButton } from "../../../components";
import { CiUser } from "react-icons/ci";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { AuthThunk } from "../../../functions/auth";
import ApiRoutes from "../../../routes/ApiRoutes";

///
export default function ForgotPasswordPage() {
  const navigation = useNavigate();
  const [username, setUsername] = useState<string>("");
  const dispatch = useAppDispatch();

  function handlePasswordReset() {
    dispatch(
      AuthThunk({
        data: { username },
        method: "post",
        url: ApiRoutes.auth.resetPassword,
      })
    );
  }

  return (
    <Stack
      height="100%"
      width="100%"
      alignItems="center"
      justifyContent="center"
      sx={(theme) => ({
        [theme.breakpoints.down("sm")]: {
          padding: theme.spacing(2),
        },
        padding: theme.spacing(4),
      })}
      spacing={2}
    >
      <Typography fontWeight={900} variant="h4">
        Forgot Password
      </Typography>
      <Typography variant="body2">
        enter email address or phone number used to register account
      </Typography>
      <Stack
        spacing={1.75}
        sx={(theme) => ({
          [theme.breakpoints.down("sm")]: {
            width: "100%",
          },
          width: "80%",
        })}
      >
        <InputGroup
          label="PhoneNumber/Email*"
          placeholder="PhoneNumber/Email"
          props={{
            InputProps: {
              startAdornment: (
                <InputAdornment position="start">
                  <CiUser />
                </InputAdornment>
              ),
            },
            type: "text",
            name: "username",
          }}
          handleChange={(e) => setUsername(e.target.value)}
        />

        {username.length > 0 && (
          <PrimaryButton
            title="Reset Password"
            handleClick={handlePasswordReset}
          />
        )}

        <Stack
          direction="row"
          alignItems="center"
          justifyContent="center"
          width="100%"
        >
          <Chip
            sx={(theme) => ({
              bgcolor: theme.palette.common.white,
              borderRadius: theme.spacing(0),
              borderWidth: 0,
            })}
            onClick={() => navigation(NavigationRoutes.account.login)}
            size="small"
            avatar={<BsArrowLeftShort />}
            label={
              <Typography color="primary" variant="body2">
                Back to login
              </Typography>
            }
          />
        </Stack>
      </Stack>
    </Stack>
  );
}
