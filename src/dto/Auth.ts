import { CountryType } from "../pages/data/country.data";

interface IAuthDto {
  name: string;
  password: string;
  userType: string;
  country: CountryType | null;
}

export interface RegisterViaEmailDto extends IAuthDto {
  email: string;
}

export interface RegisterViaPhoneDto extends IAuthDto {
  phoneNumber: string;
}

export interface ILoginWithEmailDto {
  email: string;
  password: string;
}

export interface ILoginDto {
  username: string;
  password: string;
}

export interface IResetPasswordDto {
  code: string;
  password: string;
  username: string;
  confirmPassword: string;
}

export interface IChangePasswordDto {
  oldPassword: string;
  newPassword: string;
  confirmNewPassword: string;
}
