import UserModel from "../models/UserModel";

export interface CreateGroupDto {
  title: string;
  description: string;
  createdBy: string;
  members: string[];
  admins: string[];
  tag: string;
  invites: UserModel[];
}
