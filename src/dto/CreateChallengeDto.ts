import ChallengeCategory from "../models/ChallegeCategory";
import UserModel from "../models/UserModel";

export default interface CreateChallengeDto {
  tag: string;
  title: string;
  description: string;
  category: ChallengeCategory | null;
  competitors: UserModel[];
  startDate: string;
  startTime: string;
}
