export default interface ApiResponseModel<T> {
  data: T;
  message: string;
  code: number;
}
