import { CourseDurationModel, CourseType, FileInfoModel } from ".";
import ProfileModel, { UserProfileInfo } from "./ProfileModel";
import UserModel from "./UserModel";

export interface CourseModel extends CourseDto {
  _id: string;
  courseId: string;
  createdAt: string;
  updatedAt: string;
  registeredStudents: string[];
  status: string;
  courseStatus: string;
  userId: string;
  coverImage: FileInfoModel;
  trailer?: FileInfoModel;
}

export interface CourseDto {
  courseType: CourseType;
  fee: number;
  title: string;
  description: string;
  duration: CourseDurationModel;
}

export interface CourseInfo {
  course: CourseModel;
  author: {
    user: UserModel;
    profile: ProfileModel;
  };
  students: UserProfileInfo[];
}
