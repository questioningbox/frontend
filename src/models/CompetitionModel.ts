import { Status } from "../enum/Status";
import ChallengeCategory from "./ChallegeCategory";
import ProfileModel from "./ProfileModel";
import UserModel from "./UserModel";

export default interface CompetitionModel {
  _id: string;

  title: string;

  description: string;

  tag: string;

  createdAt: string;

  updatedAt: string;

  competitionId: string;

  competitors: string[]; ///the competitors of a competition is either stuents or groups

  completed: boolean;

  status: Status;

  createdBy: string;

  category: ChallengeCategory;

  acceptedMembers: string[];

  challengeDate: string;

  startDate: string;

  startTime: string;

  request?: string[];

  members?: string[];

  admins?: string[];
}

export interface CompetitionDetails {
  _id: string;

  title: string;

  description: string;

  tag: string;

  createdAt: string;

  updatedAt: string;

  competitionId: string;

  competitors: string[]; ///the competitors of a competition is either stuents or groups

  completed: boolean;

  status: Status;

  createdBy: CompetitionMemberInfo;

  category: ChallengeCategory;

  acceptedMembers: CompetitionMemberInfo[];

  challengeDate: string;

  startDate: string;

  startTime: string;

  request: CompetitionMemberInfo[];

  members: CompetitionMemberInfo[];

  admins: string[];
}

export interface CompetitionMemberInfo {
  info: UserModel;
  profile: ProfileModel;
}
