export default interface ErrorResponseModel {
  error: string;
  statusCode: number;
  message: string[];
}
