import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Divider,
  Grid,
  IconButton,
  Paper,
  Stack,
  Typography,
  colors,
} from "@mui/material";
import React from "react";
import { MdGroup } from "react-icons/md";
import { useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import controller from "../controller";
import { setGroups } from "../features/slice/GroupsReducer";
import {
  responsePending,
  responseSuccessful,
  responseFailed,
} from "../features/slice/ResponseSlice";
import { PagedResults } from "../models";
import ApiResponseModel from "../models/ApiResponseModel";
import GroupModel from "../models/GroupModel";
import resources from "../resources";
import ApiRoutes from "../routes/ApiRoutes";
import CustomIconButton from "./CustomIconButton";
import { BiMessageAltDetail } from "react-icons/bi";
import { BsBookmarkHeart } from "react-icons/bs";
import { CiBookmarkCheck, CiBookmarkPlus } from "react-icons/ci";
import { IoMdCheckmark } from "react-icons/io";
import { FaUserTimes } from "react-icons/fa";
import { IoPersonAddOutline } from "react-icons/io5";
import { ProfileAvatar } from "../components";
import { HiOutlineBadgeCheck } from "react-icons/hi";
import { SlBadge } from "react-icons/sl";
interface IProps {
  info: GroupModel;
}
export default function GroupCard({ info }: IProps) {
  const { user } = useAppSelector((state) => state.UserReducer);
  const dispatch = useAppDispatch();
  const { groups } = useAppSelector((state) => state.GroupsReducer);
  const navigation = useNavigate();
  const { loading } = useAppSelector((state) => state.ResponseReducer);
  //
  async function handleInvite(status: string) {
    try {
      dispatch(responsePending());
      const res = await controller<ApiResponseModel<GroupModel>>({
        method: "put",
        url: ApiRoutes.group.crud(`invite/${info.groupId}/${status}`),
        token: user?.token,
      });
      dispatch(responseSuccessful(res.message));
      const data: PagedResults<GroupModel> = {
        ...groups,
        results: groups.results.map((g) =>
          g.groupId === info.groupId ? res.data : g
        ),
      };
      dispatch(setGroups(data));
    } catch (error) {
      dispatch(responseFailed(error));
    }
  }

  async function handleJoin() {
    try {
      dispatch(responsePending());
      const res = await controller<ApiResponseModel<GroupModel>>({
        method: "put",
        url: ApiRoutes.group.crud(`join/${info.groupId}`),
        token: user?.token,
      });
      dispatch(responseSuccessful(res.message));
      const data: PagedResults<GroupModel> = {
        ...groups,
        results: groups.results.map((g) =>
          g.groupId === info.groupId ? res.data : g
        ),
      };
      dispatch(setGroups(data));
    } catch (error) {
      dispatch(responseFailed(error));
    }
  }
  return (
    <Paper
      sx={(theme) => ({
        width: "100%",
      })}
      variant="outlined"
    >
      <Stack padding={1} width="100%">
        <Stack
          direction="row"
          spacing={1.5}
          alignItems="center"
          justifyContent="flex-start"
        >
          <Stack>
            <ProfileAvatar
              size="medium"
              profileUrl={(info.banner && info.banner?.url) || ""}
            />
          </Stack>
          <Stack spacing={1}>
            <Stack
              direction="row"
              spacing={1.5}
              alignItems="center"
              justifyContent="flex-start"
            >
              <Typography variant="body1">{info.title}</Typography>
              <Typography
                sx={(theme) => ({
                  fontSize: theme.spacing(1.25),
                  borderRadius: theme.spacing(2),
                  bgcolor: colors.grey[200],
                  color: theme.palette.primary.main,
                  textAlign: "center",
                  padding: theme.spacing(0, 1),
                })}
                variant="caption"
              >
                {info.tag}
              </Typography>
              {user &&
                user.userId !== info.createdBy &&
                info.members.includes(user.userId) && (
                  <Stack color={(theme) => theme.palette.primary.dark}>
                    <HiOutlineBadgeCheck fontSize="small" />
                  </Stack>
                )}
              {user && user.userId === info.createdBy && (
                <Stack color={(theme) => colors.deepOrange.A700}>
                  <SlBadge fontSize="small" />
                </Stack>
              )}
            </Stack>
            <Stack
              direction="row"
              alignItems="center"
              justifyContent="flex-start"
              spacing={1.5}
            >
              <Typography fontWeight="800" variant="body2">
                members {` ${info.members.length}`}
              </Typography>
              {user && info.createdBy !== user.userId && (
                <CustomIconButton
                  handleClick={handleJoin}
                  size="xsmall"
                  variant="outlined"
                  title="Join"
                />
              )}
              <CustomIconButton
                size="xsmall"
                variant="contained"
                title="Readmore"
              />
            </Stack>
            {user &&
              info.createdBy !== user.userId &&
              info.invites.includes(user?.userId) && (
                <Stack
                  direction="row"
                  alignItems="center"
                  justifyContent="flex-start"
                  spacing={1.5}
                >
                  <Typography variant="body1">Invite</Typography>
                  <CustomIconButton
                    title="Accept"
                    size="xsmall"
                    variant="outlined"
                    handleClick={() => handleInvite("1")}
                  />
                  <CustomIconButton
                    title="Decline"
                    size="xsmall"
                    variant="outlined"
                    handleClick={() => handleInvite("0")}
                  />
                </Stack>
              )}
          </Stack>
        </Stack>
      </Stack>
    </Paper>
    // <Grid item sm={4} xs={6} lg={3} md={4}>
    //   <Card
    //     elevation={0}
    //     sx={(theme) => ({
    //       borderRadius: 0,
    //     })}
    //     variant="outlined"
    //   >
    //     <CardMedia
    //       sx={{ height: 140 }}
    //       image={info.banner ? info.banner.url : resources.banner.toString()}
    //       title="green iguana"
    //     />
    //     <CardContent>
    //       <Typography gutterBottom variant="h6" component="div">
    //         {info.tag}
    //       </Typography>
    //       <Divider />
    //       <Typography
    //         textAlign="justify"
    //         component="body"
    //         color="text.secondary"
    //       >
    //         {info.title}
    //       </Typography>
    //     </CardContent>
    //     <CardActions>
    //       <Stack
    //         flex={1}
    //         alignItems="center"
    //         justifyContent="flex-start"
    //         direction="row"
    //         spacing={0.5}
    //       >
    //         <MdGroup />
    //         <Typography variant="body2">{info.members.length}</Typography>
    //       </Stack>

    //       {user &&
    //         !info.invites.includes(user?.userId.toString()) &&
    //         info.createdBy !== user?.userId && (
    //           <CustomIconButton
    //             size="xsmall"
    //             variant="contained"
    //             title="Join"
    //             handleClick={handleJoin}
    //           />
    //         )}
    //       {user && info.invites.includes(user?.userId) && (
    //         <IconButton
    //           onClick={() => (loading ? {} : handleInvite("0"))}
    //           size="small"
    //         >
    //           <FaUserTimes style={{ color: "red" }} />
    //         </IconButton>
    //       )}
    //       {user && info.invites.includes(user?.userId.toString()) && (
    //         <IconButton
    //           onClick={() => (loading ? {} : handleInvite("1"))}
    //           size="small"
    //         >
    //           <IoPersonAddOutline color="green" />
    //         </IconButton>
    //       )}
    //     </CardActions>
    //   </Card>
    // </Grid>
  );
}
