import * as React from "react";
import Button from "@mui/material/Button";
import Dialog, { DialogProps } from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import Slide from "@mui/material/Slide";
import { TransitionProps } from "@mui/material/transitions";

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

interface IProps {
  props?: DialogProps;
  open: boolean;
  children: React.ReactNode;
  Header?: React.ReactNode;
  DialogAction?: React.ReactNode;
}
export default function CustomModal({
  open,
  props,
  children,
  Header,
  DialogAction,
}: IProps) {
  return (
    <Dialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      {...props}
      aria-describedby="alert-dialog-slide-description"
    >
      <DialogTitle>{Header}</DialogTitle>
      <DialogContent dividers>{children}</DialogContent>
      <DialogActions>{DialogAction}</DialogActions>
    </Dialog>
  );
}
