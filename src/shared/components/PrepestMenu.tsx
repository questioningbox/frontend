import { Menu, MenuItem } from "@mui/material";
import React from "react";

interface IProps {
  anchorEl: HTMLElement | null;
  handleClose: () => void;
}
export default function PrepestMenu({ anchorEl, handleClose }: IProps) {
  return (
    <Menu open={Boolean(anchorEl)} anchorEl={anchorEl} onClose={handleClose}>
      <MenuItem onClick={handleClose}>View All</MenuItem>
      <MenuItem onClick={handleClose}>Design</MenuItem>
      <MenuItem onClick={handleClose}>Products</MenuItem>
      <MenuItem onClick={handleClose}>Engineering</MenuItem>
      <MenuItem onClick={handleClose}>Services</MenuItem>
    </Menu>
  );
}
