import React, { useEffect, useState } from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import { ProfileAvatar } from "../components";
import { Divider, IconButton, Stack, Typography } from "@mui/material";
import { IoMdClose } from "react-icons/io";
import ProfileModel from "../models/ProfileModel";
import { useAppDispatch } from "../app/hooks";
import {
  responseFailed,
  responsePending,
  responseSuccessful,
} from "../features/slice/ResponseSlice";
import { UserInfo } from "../models/UserModel";
import controller from "../controller";
import ApiResponseModel from "../models/ApiResponseModel";
import ApiRoutes from "../routes/ApiRoutes";
import {
  EducationalInfoCard,
  WorkExperienceInfoCard,
} from "../pages/student/components";

interface IProps {
  open: boolean;
  handleClose: () => void;
  profile: ProfileModel;
}
export default function ProfileViewModal({
  open,
  handleClose,
  profile,
}: IProps) {
  const dispatch = useAppDispatch();
  const [userInfo, setUserInfo] = useState<UserInfo>({
    name: "",
    username: "",
    phoneNumber: "",
    email: "",
    userType: "",
  });

  async function handleUserInfo() {
    try {
      dispatch(responsePending());
      const info = await controller<ApiResponseModel<UserInfo>>({
        data: null,
        method: "get",
        token: "",
        url: ApiRoutes.auth.userInfo(profile.userId),
      });
      setUserInfo({ ...info.data });
      dispatch(responseSuccessful(null));
    } catch (error) {
      dispatch(responseFailed(error));
    }
  }

  useEffect(() => {
    handleUserInfo();
  }, []);
  return (
    <Dialog PaperComponent={Stack} open={open} fullWidth maxWidth="sm">
      <DialogTitle
        sx={(theme) => ({
          backgroundColor: theme.palette.common.white,
          boxShadow: theme.shadows[1],
        })}
      >
        <Stack
          spacing={1}
          alignItems="center"
          justifyContent="flex-start"
          direction="row"
        >
          <ProfileAvatar
            name={!profile.profileImage ? userInfo.name : ""}
            profileUrl={profile?.profileImage.secureUrl}
            size="medium"
          />
          <Stack>
            <Typography variant="body1" fontWeight="bold">
              {userInfo.name}
            </Typography>
            <Typography variant="caption">{userInfo.username}</Typography>
          </Stack>
          <Stack flex={1} />
          <IconButton onClick={handleClose} size="small">
            <IoMdClose />
          </IconButton>
        </Stack>
      </DialogTitle>
      <DialogContent
        sx={(theme) => ({
          backgroundColor: theme.palette.common.white,
          boxShadow: theme.shadows[1],
        })}
        dividers
      >
        <Stack spacing={1}>
          <Stack spacing={1} direction="row">
            <Stack direction="row" spacing={0.5}>
              <Typography variant="caption">Followers:</Typography>
              <Typography variant="caption">
                {profile.followers.length}
              </Typography>
            </Stack>
            <Stack direction="row" spacing={0.5}>
              <Typography variant="caption">Following:</Typography>
              <Typography variant="caption">
                {profile.following.length}
              </Typography>
            </Stack>
          </Stack>
          {profile.educationalInfo.length > 0 && (
            <Stack>
              <Divider />
              <Typography variant="body1">Educational Info</Typography>
              {profile.educationalInfo.map((eduInfo) => (
                <EducationalInfoCard
                  handleRemove={() => {}}
                  handleEdit={() => {}}
                  key={eduInfo.id}
                  info={eduInfo}
                />
              ))}
            </Stack>
          )}
          {profile.workExperience.length > 0 && (
            <Stack>
              <Divider />
              <Typography variant="body1">Work Experience Info</Typography>
              {profile.workExperience.map((work) => (
                <WorkExperienceInfoCard
                  handleRemove={() => {}}
                  handleEdit={() => {}}
                  key={work.id}
                  info={work}
                />
              ))}
            </Stack>
          )}
        </Stack>
      </DialogContent>
      <DialogActions
        sx={(theme) => ({
          backgroundColor: theme.palette.common.white,
          boxShadow: theme.shadows[1],
        })}
      ></DialogActions>
    </Dialog>
  );
}
