import dayjs from "dayjs";
import { faker } from "@faker-js/faker";

export interface IStoreItem {
  author: string;
  dateAdded: string;
  tag: string;
  title: string;
  body: string;
  id: number;
  image: string;
}

export function getStoreItems(length: number = 50): IStoreItem[] {
  const data: IStoreItem[] = [];
  [...Array.from({ length })].map((_, i) => {
    data.push({
      id: i + 1,
      author: faker.name.fullName({ gender: i % 2 ? "male" : "female" }),
      body: faker.commerce.productDescription(),
      image: faker.image.fashion(500, 500, true),
      tag: faker.commerce.productAdjective(),
      dateAdded: dayjs().format(),
      title: faker.commerce.product(),
    });
  });
  return data;
}

export interface IFeed {
  username: string;
  message: string;
  dateAdded: string;
  profileURL: string;
  id: number;
}

export function getFeeds(length = 25, daysDiff = 0): IFeed[] {
  const data: IFeed[] = [];
  [...Array.from({ length })].map((_, i) => {
    data.push({
      id: i + 1,
      username: faker.name.firstName(),
      dateAdded: dayjs().subtract(daysDiff, "days").format(),
      profileURL: faker.image.fashion(360, 360, true),
      message: faker.lorem.words(10),
    });
  });
  return data;
}

export function getCategories(length = 7): string[] {
  const data: string[] = [];
  Array.from({ length }).map(() => {
    data.push(faker.commerce.productMaterial());
  });

  return data;
}
