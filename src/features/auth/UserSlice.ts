import { PagedResults } from "./../../models/index";
import { createSlice } from "@reduxjs/toolkit";
import { UserReducerState } from "../../app/state";
import { AuthThunk, LogoutThunk } from "../../functions/auth";
import { UserAnalyticsThunk, UserProfileThunk } from "../../functions/slice";
import UserModel from "../../models/UserModel";
import { UserProfileInfo } from "../../models/ProfileModel";

const UserSlice = createSlice({
  name: "UserReducer",
  initialState: UserReducerState,
  reducers: {
    userLogout: (state) => {
      state.user = null;
    },
    setUsers: (state, action: { payload: UserModel[] }) => {
      state.users = action.payload;
    },
    setProfiles: (
      state,
      action: { payload: PagedResults<UserProfileInfo> }
    ) => {
      state.profiles = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(AuthThunk.fulfilled, (state, action) => {
        state.user = action.payload.data;
      })
      .addCase(LogoutThunk.fulfilled, (state, action) => {
        state.user = action.payload.data;
      })
      .addCase(UserProfileThunk.fulfilled, (state, action) => {
        state.profile = action.payload.data;
      })
      .addCase(UserAnalyticsThunk.fulfilled, (state, action) => {
        state.analytics = action.payload.data;
      });
  },
});

export default UserSlice.reducer;

export const { userLogout, setUsers, setProfiles } = UserSlice.actions;
