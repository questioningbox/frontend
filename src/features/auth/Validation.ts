import {
  ILoginDto,
  ILoginWithEmailDto,
  IResetPasswordDto,
  RegisterViaPhoneDto,
} from "./../../dto/Auth";
/* eslint-disable no-throw-literal */
import { RegisterViaEmailDto } from "./../../dto/Auth";

export interface IValidationError {
  name: string;
  error: any;
}

const phoneRegex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;
const emailRegex =
  /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;
const passwordRegex =
  /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,}$/;
export function ValidateRegisterViaEmailDto(info: RegisterViaEmailDto) {
  if (info.name.length <= 0) {
    throw { name: "name", error: "Name required, please enter your full name" };
  }
  if (!emailRegex.test(info.email)) {
    throw { name: "email", error: "Invalid Email Address" };
  }
  if (!passwordRegex.test(info.password)) {
    throw {
      name: "password",
      error:
        "invalid password,Minimum eight characters, at least one upper case letter, one lower case letter, one number and one special character",
    };
  }
}

export function ValidateRegisterViaPhoneDto(info: RegisterViaPhoneDto) {
  if (info.name.length <= 0) {
    throw {
      name: "name",
      error: "Name required, please enter your full name",
    };
  }
  if (!phoneRegex.test(info.phoneNumber)) {
    throw { name: "phone", error: "Invalid Phone Number" };
  }
  if (!passwordRegex.test(info.password)) {
    throw {
      name: "password",
      error:
        "invalid password,Minimum eight characters, at least one upper case letter, one lower case letter, one number and one special character",
    };
  }
}

export function ValidateLoginInfo(info: ILoginDto) {
  if (info.username.length <= 0) {
    throw {
      name: "username",
      error: "enter email address or phone number",
    };
  }
  if (info.password.length <= 0) {
    throw {
      name: "password",
      error: "enter password",
    };
  }
}

export function ValidateResetPasswordInfo(info: IResetPasswordDto) {
  if (info.code.length <= 0) {
    throw "Enter Verification Code";
  }
  if (!passwordRegex.test(info.password)) {
    throw "invalid password,Minimum eight characters, at least one upper case letter, one lower case letter, one number and one special character";
  }
  if (!(info.password === info.confirmPassword)) {
    throw "password doesn't match";
  }
}
