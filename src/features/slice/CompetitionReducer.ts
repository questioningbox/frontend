import { createSlice } from "@reduxjs/toolkit";
import { CompetitionsReducerState } from "../../app/state";
import { CompetitionsThunk } from "../../functions/slice";
import { PagedResults } from "../../models";
import CompetitionModel from "../../models/CompetitionModel";

const competitionReducer = createSlice({
  name: "CompetitionsReducer",
  initialState: CompetitionsReducerState,
  reducers: {
    setCompetitions: (
      state,
      action: { payload: PagedResults<CompetitionModel> }
    ) => {
      state.competitions = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(CompetitionsThunk.pending, (state) => {
        state.loading = true;
        state.error = null;
        state.message = null;
      })
      .addCase(CompetitionsThunk.fulfilled, (state, action) => {
        state.loading = false;
        state.competitions = action.payload.data;
        state.error = null;
        state.message = action.payload.message;
      })
      .addCase(CompetitionsThunk.rejected, (state, action) => {
        state.error = action.error.message || action.payload;
        state.loading = false;
        state.message = null;
      });
  },
});

export const { setCompetitions } = competitionReducer.actions;
export default competitionReducer.reducer;
