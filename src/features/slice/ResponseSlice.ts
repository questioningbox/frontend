import { createSlice } from "@reduxjs/toolkit";
import { ResponseReducerState } from "../../app/state";
import { AuthThunk, LogoutThunk } from "../../functions/auth";
import {
  ContestsThunk,
  ContestThunk,
  CourseThunk,
  FeedsThunk,
  GetContestQuestionThunk,
  GroupsThunk,
  GroupThunk,
  UserAnalyticsThunk,
  UserProfileThunk,
} from "../../functions/slice";

const ResponseSlice = createSlice({
  name: "ResponseReducer",
  initialState: ResponseReducerState,
  reducers: {
    responsePending: (state) => {
      state.loading = true;
      state.error = null;
      state.message = null;
    },
    responseFailed: (state, action) => {
      state.error = action.payload;
      state.message = null;
      state.loading = false;
    },
    responseSuccessful: (state, action) => {
      state.error = null;
      state.message = action.payload;
      state.loading = false;
    },
    clearResponse: (state) => {
      state.error = null;
      state.message = null;
      state.loading = false;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(AuthThunk.fulfilled, (state, action) => {
        state.message = action.payload.message;
        state.error = null;
        state.loading = false;
      })
      .addCase(AuthThunk.pending, (state) => {
        state.loading = true;
        state.message = null;
        state.error = null;
      })
      .addCase(AuthThunk.rejected, (state, action) => {
        state.error = action.error.message;
        state.message = null;
        state.loading = false;
      })
      //
      .addCase(LogoutThunk.fulfilled, (state, action) => {
        state.message = action.payload.message;
        state.error = null;
        state.loading = false;
      })
      .addCase(LogoutThunk.pending, (state) => {
        state.loading = true;
        state.message = null;
        state.error = null;
      })
      .addCase(LogoutThunk.rejected, (state, action) => {
        state.error = action.error.message;
        state.message = null;
        state.loading = false;
      })
      //
      .addCase(UserProfileThunk.fulfilled, (state, action) => {
        state.message = action.payload.message;
        state.error = null;
        state.loading = false;
      })
      .addCase(UserProfileThunk.pending, (state) => {
        state.loading = true;
        state.message = null;
        state.error = null;
      })
      .addCase(UserProfileThunk.rejected, (state, action) => {
        state.error = action.error.message;
        state.message = null;
        state.loading = false;
      })
      //
      .addCase(FeedsThunk.fulfilled, (state, action) => {
        state.message = action.payload.message;
        state.error = null;
        state.loading = false;
      })
      .addCase(FeedsThunk.pending, (state) => {
        state.loading = true;
        state.message = null;
        state.error = null;
      })
      .addCase(FeedsThunk.rejected, (state, action) => {
        state.error = action.error.message;
        state.message = null;
        state.loading = false;
      })
      //
      .addCase(CourseThunk.fulfilled, (state, action) => {
        state.message = action.payload.message;
        state.error = null;
        state.loading = false;
      })
      .addCase(CourseThunk.pending, (state) => {
        state.loading = true;
        state.message = null;
        state.error = null;
      })
      .addCase(CourseThunk.rejected, (state, action) => {
        state.error = action.error.message;
        state.message = null;
        state.loading = false;
      })
      //
      .addCase(GroupThunk.fulfilled, (state, action) => {
        state.message = action.payload.message;
        state.error = null;
        state.loading = false;
      })
      .addCase(GroupThunk.pending, (state) => {
        state.loading = true;
        state.message = null;
        state.error = null;
      })
      .addCase(GroupThunk.rejected, (state, action) => {
        state.error = action.error.message;
        state.message = null;
        state.loading = false;
      })
      //
      .addCase(GroupsThunk.fulfilled, (state, action) => {
        state.message = action.payload.message;
        state.error = null;
        state.loading = false;
      })
      .addCase(GroupsThunk.pending, (state) => {
        state.loading = true;
        state.message = null;
        state.error = null;
      })
      .addCase(GroupsThunk.rejected, (state, action) => {
        state.error = action.error.message;
        state.message = null;
        state.loading = false;
      })
      //
      .addCase(GetContestQuestionThunk.pending, (state) => {
        state.loading = true;
        state.error = null;
        state.message = null;
      })
      .addCase(GetContestQuestionThunk.fulfilled, (state) => {
        state.loading = false;
        state.error = null;
        state.message = null;
      })
      .addCase(GetContestQuestionThunk.rejected, (state) => {
        state.loading = false;
        state.error = null;
        state.message = null;
      })
      //
      .addCase(ContestThunk.pending, (state) => {
        state.loading = true;
        state.error = null;
        state.message = null;
      })
      .addCase(ContestThunk.fulfilled, (state) => {
        state.loading = false;
        state.error = null;
        state.message = null;
      })
      .addCase(ContestThunk.rejected, (state) => {
        state.loading = false;
        state.error = null;
        state.message = null;
      })
      //
      .addCase(ContestsThunk.pending, (state) => {
        state.loading = true;
        state.error = null;
        state.message = null;
      })
      .addCase(ContestsThunk.fulfilled, (state) => {
        state.loading = false;
        state.error = null;
        state.message = null;
      })
      .addCase(ContestsThunk.rejected, (state) => {
        state.loading = false;
        state.error = null;
        state.message = null;
      })
      //
      .addCase(UserAnalyticsThunk.pending, (state) => {
        state.loading = true;
        state.error = null;
        state.message = null;
      })
      .addCase(UserAnalyticsThunk.fulfilled, (state) => {
        state.loading = false;
        state.error = null;
        state.message = null;
      })
      .addCase(UserAnalyticsThunk.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error?.message || action.error || action.payload;
        state.message = null;
      });
  },
});

export default ResponseSlice.reducer;

export const {
  responseFailed,
  responsePending,
  responseSuccessful,
  clearResponse,
} = ResponseSlice.actions;
