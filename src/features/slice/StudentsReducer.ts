import { PagedResults } from "./../../models/index";
import { createSlice } from "@reduxjs/toolkit";
import { StudentsReducerState } from "../../app/state";
import { UserProfileInfo } from "../../models/ProfileModel";
import { StudentsThunk } from "../../functions/slice";

const studentsReducer = createSlice({
  name: "StudentsReducer",
  initialState: StudentsReducerState,
  reducers: {
    setStudents: (
      state,
      action: { payload: PagedResults<UserProfileInfo> }
    ) => {
      state.students = action.payload;
    },
    setStudentsLoading: (state) => {
      state.loading = true;
      state.error = null;
      state.message = null;
    },
    setStudentsError: (state, action) => {
      state.error = action.payload;
      state.message = null;
      state.loading = false;
    },
    setStudentsMessage: (state, action) => {
      state.error = null;
      state.message = action.payload;
      state.loading = false;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(StudentsThunk.pending, (state) => {
        state.error = null;
        state.message = null;
        state.loading = true;
      })
      .addCase(StudentsThunk.fulfilled, (state, action) => {
        state.error = null;
        state.message = action.payload.message;
        state.loading = false;
        state.students = action.payload.data;
      })
      .addCase(StudentsThunk.rejected, (state, action) => {
        state.error = action.error.message || action.error || action.payload;
        state.loading = false;
        state.message = null;
      });
  },
});

export const {
  setStudents,
  setStudentsError,
  setStudentsLoading,
  setStudentsMessage,
} = studentsReducer.actions;
export default studentsReducer.reducer;
