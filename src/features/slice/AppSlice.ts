import { createSlice } from "@reduxjs/toolkit";
import { AppReducerState } from "../../app/state";

const AppSlice = createSlice({
  name: "AppReducer",
  initialState: AppReducerState,
  reducers: {
    setFeed: (state) => {
      state.feed = !state.feed;
    },
  },
});

export default AppSlice.reducer;
export const { setFeed } = AppSlice.actions;
