import { createSlice } from "@reduxjs/toolkit";
import { GroupsReducerState } from "../../app/state";
import { GroupsThunk, GroupThunk } from "../../functions/slice";
import { PagedResults } from "../../models";
import GroupModel from "../../models/GroupModel";

const GroupsReducer = createSlice({
  name: "GroupsReducer",
  initialState: GroupsReducerState,
  reducers: {
    setGroup: (state, action) => {
      state.group = action.payload;
    },
    setGroups: (state, action: { payload: PagedResults<GroupModel> }) => {
      state.groups = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(GroupsThunk.fulfilled, (state, action) => {
        state.groups = action.payload.data;
      })
      .addCase(GroupThunk.fulfilled, (state, action) => {
        state.group = action.payload.data;
      });
  },
});

export const { setGroups, setGroup } = GroupsReducer.actions;

export default GroupsReducer.reducer;
