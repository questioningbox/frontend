import { Button, Stack, Typography, useMediaQuery } from "@mui/material";
import React from "react";
import { PrimaryLinkButton } from "../components";

export default function ServiceUpgradeView() {
  const isMobile = useMediaQuery("(max-width:567px)");
  return (
    <Stack padding={(theme) => theme.spacing(2)} width="100%">
      <Stack
        direction={isMobile ? "column" : "row"}
        alignItems="center"
        justifyContent="space-between"
        padding={(theme) => theme.spacing(3, 4)}
        borderRadius={(theme) => theme.spacing(1)}
        bgcolor={(theme) => theme.palette.action.hover}
        spacing={1}
      >
        <Stack alignItems="flex-start" justifyContent="center" flex={1}>
          <Typography
            fontSize={(theme) => theme.spacing(2.35)}
            textAlign="left"
            variant="body1"
            fontWeight={900}
          >
            Upgrade to premium
          </Typography>
          <Typography textAlign="left" variant="caption">
            Upgrade to get access to all features now
          </Typography>
        </Stack>
        <PrimaryLinkButton route="" text="Upgrade now" />
      </Stack>
    </Stack>
  );
}
