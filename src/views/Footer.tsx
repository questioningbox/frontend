import { Stack, Typography } from "@mui/material";
import dayjs from "dayjs";
import React from "react";
import { BiEnvelope } from "react-icons/bi";

export default function Footer() {
  return (
    <Stack
      direction="row"
      padding={1.5}
      width="100%"
      alignItems="center"
      justifyContent="space-between"
      spacing={1}
      sx={(theme) => ({
        [theme.breakpoints.down("sm")]: {
          marginBotton: theme.spacing(10),
        },
      })}
    >
      <Stack direction="row" alignItems="center" justifyContent="flex-start">
        <Typography variant="body2">
          &copy; QuizBox {dayjs().format("YYYY")}
        </Typography>
      </Stack>
      <Stack
        direction="row"
        spacing={1}
        alignItems="center"
        justifyContent="flex-end"
      >
        <BiEnvelope />
        <Typography variant="body2">questioningbox@gmail.com</Typography>
      </Stack>
    </Stack>
  );
}
