import { IconButton, Stack } from "@mui/material";
import React from "react";
import { CategoryCard } from "../pages/home/components";
import { HomeCategories } from "../pages/home/data";
import { BsArrowRightShort } from "react-icons/bs";
import { AppColors } from "../constants";
export default function HomeCategoryView() {
  return (
    <Stack
      direction="row"
      alignItems="center"
      justifyContent="space-between"
      width="100%"
      padding={(theme) => theme.spacing(1, 1.5)}
      spacing={1.5}
    >
      {HomeCategories.map((cate) => (
        <CategoryCard info={cate} key={cate.title} />
      ))}
      <Stack flex={1} />
      <Stack
        height="100%"
        alignItems="center"
        paddingBottom={2}
        justifyContent="center"
      ></Stack>
    </Stack>
  );
}
