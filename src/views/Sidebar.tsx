import { useQuery } from "@chakra-ui/react";
import { Divider, Drawer, IconButton, Stack, Typography } from "@mui/material";
import React, { useState } from "react";
import { BiCog } from "react-icons/bi";
import { RiAccountPinCircleFill, RiSettingsLine } from "react-icons/ri";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { SidebarLink } from "../components";
import { AppColors } from "../constants";
import ISidebarLink from "../data/data";
import { avatarImages } from "../resources";
import NavigationRoutes from "../routes/NavigationRoutes";
import { AccountProfileMenu } from "../shared/components";

interface IProps {
  handleSidebar: () => void;
  sidebar: boolean;
  routes: ISidebarLink[];
}
export default function Sidebar({ sidebar, handleSidebar, routes }: IProps) {
  const navigation = useNavigate();
  const path = useLocation();
  const [accountProfileMenu, setAccountProfileMenu] =
    useState<HTMLElement | null>(null);
  return (
    <Drawer variant="persistent" open={sidebar}>
      <AccountProfileMenu
        anchorEl={accountProfileMenu}
        handleClose={() => setAccountProfileMenu(null)}
      />
      <Stack
        width="80px"
        bgcolor={(theme) => theme.palette.background.default}
        height="100vh"
      >
        <Stack alignItems="center" justifyContent="center" height="60px">
          <Typography
            variant="body1"
            fontWeight={900}
            fontSize={(theme) => theme.spacing(3)}
          >
            Logo
          </Typography>
        </Stack>
        <Divider />
        <Stack height="100%" padding={1}>
          <Stack
            spacing={1.5}
            width="100%"
            alignItems="center"
            justifyContent="flex-start"
            height="100%"
            flex={1}
          >
            {routes.map((link) => (
              <SidebarLink
                handleClick={() =>
                  link.route ? navigation(link.route) : () => {}
                }
                info={link}
                key={link.title}
              />
            ))}
          </Stack>
          <Stack alignItems="center" justifyContent="center" spacing={1}>
            <IconButton
              onClick={() => navigation(NavigationRoutes.student.profile)}
            >
              <RiSettingsLine
                style={{
                  color: [
                    NavigationRoutes.student.general,
                    NavigationRoutes.student.edit,
                    NavigationRoutes.student.notifications,
                    NavigationRoutes.student.profile,
                  ].includes(path.pathname)
                    ? AppColors.primary
                    : "inherit",
                }}
                size={18}
              />
            </IconButton>
            <IconButton
              onClick={(e) => setAccountProfileMenu(e.currentTarget)}
              size="medium"
              color="primary"
            >
              <RiAccountPinCircleFill style={{ color: "inherit" }} />
            </IconButton>
          </Stack>
        </Stack>
      </Stack>
    </Drawer>
  );
}
