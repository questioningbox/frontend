export default {
  feed: {
    add: "feed-new",
    comment: "feed-comment",
    delete: "feed-delete",
  },
};
