// eslint-disable-next-line import/no-anonymous-default-export
export default {
  account: {
    root: "/account",
    register: "/account/sign-up",
    login: "/account/sign-in",
    registerByPhone: "/account/phone/sign-up",
    forgotPassword: "/account/password/forgot",
    resetPassword: "/account/password/reset",
    otpVerification: "/account/otp/verify",
  },
  home: {
    root: "/",
    landing: "",
    prepTest: "/preptest",
    questions: "/questions",
    students: "/students",
    teachers: "/teachers",
    course: "/course",
  },
  preptest: {
    root: "/preptest",
    guide: "/preptest/guide",
    exams: "/preptest/exams",
    results: "/preptest/results",
  },
  pricing: {
    root: "/pricing",
  },
  questions: {
    root: "/questions",
    myquestions: "/questions/myquestions",
  },
  groups: {
    root: "/groups",
    info: "/groups/info",
  },
  student: {
    profile: "/profile",
    general: "/profile/general",
    edit: "/profile/edit",
    notifications: "/profile/email/notifications",
  },
  challenge: {
    all: "/challenges",
    sent: "/challenges/sent",
    received: "/challenges/received",
    one2one: "/challenge/friend",
    one2many: "/challenge/group",
    leaderboard: "/challenge/leaderboard",
    parcipants: "/challenge/parcipants",
    details: "/challenge/datails",
  },
  profile: {
    root: "/profile",
  },
};
