import controller, { IController } from "./../../controller/index";
import { createAsyncThunk } from "@reduxjs/toolkit";
import UserModel from "../../models/UserModel";
import ApiResponseModel from "../../models/ApiResponseModel";

export default createAsyncThunk(
  "api/user/authentication",
  async ({ data, method, url, params, token, isFile }: IController) => {
    try {
      return await controller<ApiResponseModel<UserModel>>({
        data,
        method,
        url,
        params,
        token,
        isFile,
      });
    } catch (error) {
      throw error;
    }
  }
);
