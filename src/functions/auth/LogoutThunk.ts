import { createAsyncThunk } from "@reduxjs/toolkit";
import controller, { IController } from "../../controller";
import ApiResponseModel from "../../models/ApiResponseModel";
import UserModel from "../../models/UserModel";

export default createAsyncThunk(
  "api/auth/logout",
  async ({ data, method, url, token }: IController) => {
    try {
      return await controller<ApiResponseModel<UserModel | null>>({
        method,
        url,
        data,
        token,
      });
    } catch (error) {
      throw error;
    }
  }
);
