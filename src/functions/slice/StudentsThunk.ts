import { PagedResults } from "./../../models/index";
import controller, { IController } from "./../../controller/index";
import { createAsyncThunk } from "@reduxjs/toolkit";
import ApiResponseModel from "../../models/ApiResponseModel";
import { UserProfileInfo } from "../../models/ProfileModel";

export default createAsyncThunk(
  "api/students/all",
  async ({ data, url, token, method, params }: IController) => {
    try {
      return await controller<ApiResponseModel<PagedResults<UserProfileInfo>>>({
        method,
        url,
        token,
        params,
        data,
      });
    } catch (error) {
      throw error;
    }
  }
);
