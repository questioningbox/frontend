import controller, { IController } from "./../../controller/index";
import { createAsyncThunk } from "@reduxjs/toolkit";
import ApiResponseModel from "../../models/ApiResponseModel";
import { PagedResults } from "../../models";
import { UserProfileInfo } from "../../models/ProfileModel";

export default createAsyncThunk(
  "api/teachers/all",
  async ({ method, url, token, params, data }: IController) => {
    try {
      return await controller<ApiResponseModel<PagedResults<UserProfileInfo>>>({
        method,
        url,
        token,
        params,
        data,
      });
    } catch (error) {
      throw error;
    }
  }
);
