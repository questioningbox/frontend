import { createAsyncThunk } from "@reduxjs/toolkit";
import controller, { IController } from "../../controller";
import ApiResponseModel from "../../models/ApiResponseModel";
import FeedModel from "../../models/FeedModel";

export default createAsyncThunk(
  "api/feeds",
  async ({ data, method, url, isFile, token }: IController) => {
    try {
      return await controller<ApiResponseModel<FeedModel[]>>({
        data,
        method,
        token,
        url,
        isFile,
      });
    } catch (error) {
      throw error;
    }
  }
);
