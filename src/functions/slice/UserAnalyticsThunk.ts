import { createAsyncThunk } from "@reduxjs/toolkit";
import controller, { IController } from "../../controller";
import ApiResponseModel from "../../models/ApiResponseModel";
import { UserAnalytics } from "../../models/UserModel";

export default createAsyncThunk(
  "api/user/analytics",
  async ({ token }: IController) => {
    try {
      return await controller<ApiResponseModel<UserAnalytics>>({
        method: "get",
        url: "profile/analytics",
        token,
      });
    } catch (error) {
      throw error;
    }
  }
);
