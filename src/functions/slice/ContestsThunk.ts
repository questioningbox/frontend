import controller, { IController } from "./../../controller/index";
import { createAsyncThunk } from "@reduxjs/toolkit";
import ApiResponseModel from "../../models/ApiResponseModel";
import ContestModel from "../../models/ContestModel";

export default createAsyncThunk(
  "api/constests",
  async ({ data, method, url, params, token }: IController) => {
    try {
      return await controller<ApiResponseModel<ContestModel[]>>({
        data,
        url,
        method,
        params,
        token,
      });
    } catch (error) {
      throw error;
    }
  }
);
